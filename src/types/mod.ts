// deno-lint-ignore-file camelcase

export * as m from "./events/m.ts";
import * as m from "./events/m.ts";

// deno-lint-ignore ban-types
export type APIResponse<T> = (T extends void ? {} : T) & {
    _response: Response;
};

export type FileAPIResponse = {
    file: File;
    response: Response;
};

export interface Signatures {
    [name: string]: {
        [key: string]: string;
    };
}

export interface EncryptedFile {
    /** The URL to the file. */
    url: string;
    /** A JSON Web Key object. */
    key: JWK;
    /** The 128-bit unique counter block used by AES-CTR, encoded as unpadded base64. */
    iv: string;
    /**
     * A map from an algorithm name to a hash of the ciphertext, encoded as unpadded base64.
     * Clients should support the SHA-256 hash, which uses the key `sha256`.
     */
    hashes: {
        [key: string]: string;
    };
    /**
     * Version of the encrypted attachments protocol.
     * Must be `v2`.
     */
    v: string;
}

export interface JWK {
    /**
     * Key type.
     * Must be `oct`.
     */
    kty: "oct";
    /**
     * Key operations.
     * Must at least contain `encrypt` and `decrypt`.
     */
    key_ops: string[];
    /**
     * Algorithm.
     * Must be `A256CTR`.
     */
    alg: "A256CTY";
    /** The key, encoded as urlsafe unpadded base64. */
    k: string;
    /**
     * Extractable.
     * Must be `true`.
     * This is a W3C extension.
     */
    ext: true;
}

export interface PushRule {
    /** The actions to perform when this rule is matched.  */
    actions?: any[];
    /**
     * The conditions that must hold true for an event in order for a rule to be applied to an event.
     * A rule with no conditions always matches.
     * Only applicable to `underride` and `override` rules.
     */
    conditions?: {
        /**
         * Required for `room_member_count` conditions.
         * A decimal integer optionally prefixed by one of, ==, <, >, >= or <=.
         * A prefix of < matches rooms where the member count is strictly less than the given number and so forth.
         * If no prefix is present, this parameter defaults to ==.
         */
        is?: string;
        /**
         * Required for `event_match` conditions.
         * The dot- separated field of the event to match.
         *
         * Required for `sender_notification_permission` conditions.
         * The field in the power level event the user needs a minimum power level for.
         * Fields must be specified under the `notifications` property in the power level event's `content`.
         */
        key?: string;
        /**
         * The kind of condition to apply.
         * See conditions for more information on the allowed kinds and how they work.
         */
        kind: string;
        /**
         * Required for `event_match` conditions.
         * The glob- style pattern to match against.
         * Patterns with no special glob characters should be treated as having asterisks prepended and appended when testing the condition.
         */
        pattern?: string;
    }[];
    /** Whether this is a default rule, or has been set explicitly. */
    default: boolean;
    /** Whether the push rule is enabled or not. */
    enabled: boolean;
    /** The glob-style pattern to match against. Only applicable to `content` rules. */
    pattern?: string;
    /** The ID of this rule. */
    rule_id: string;
}

export type RoomStateEvents =
    | m.room.canonical_alias
    | m.room.create
    | m.room.join_rules
    | m.room.member
    | m.room.power_levels
    | m.room.encryption
    | m.room.guest_access
    | m.room.history_visibility
    | m.room.name
    | m.room.pinned_events
    | m.room.server_acl
    | m.room.third_party_invite
    | m.room.tombstone
    | m.room.topic
    | m.policy.rule.room
    | m.policy.rule.user
    | m.policy.rule.server;
export type Message =
    | m.room.redaction
    | m.call.invite
    | m.call.candidates
    | m.call.answer
    | m.call.hangup
    | m.sticker
    | m.server_notice
    | m.text
    | m.emote
    | m.notice
    | m.image
    | m.file
    | m.audio
    | m.location
    | m.video;
export type RoomEvents = RoomStateEvents | Message | m.room.encrypted | m.typing | m.receipt | m.presence | m.fully_read;
export type Events =
    | RoomEvents
    | m.identity_server
    | m.accepted_terms
    | m.direct
    | m.dummy
    | m.forwarded_room_key
    | m.ignored_user_list
    | m.push_rules
    | m.room_key
    | m.room_key_request
    | m.key.verification.request
    | m.key.verification.accept
    | m.key.verification.cancel
    | m.key.verification.start
    | m.key.verification.key
    | m.key.verification.mac;

export interface Event<Type extends string = string, Content = any> {
    /**
     * The fields in this object will vary depending on the type of event.
     * When interacting with the REST API, this is the HTTP body.
     */
    content: Content;
    /**
     * The type of event.
     * This SHOULD be namespaced similar to Java package naming conventions e.g. 'com.example.subdomain.event.type'
     */
    type: Type;
}

export interface RoomEvent<Type extends string = string, Content = any> extends Event<Type, Content> {
    /** The globally unique event identifier. */
    event_id: string;
    /** Contains the fully-qualified ID of the user who sent this event. */
    sender: string;
    /** Timestamp in milliseconds on originating homeserver when this event was sent. */
    origin_server_ts: number;
    /** Contains optional extra information about the event. */
    unsigned?: {
        /**
         * The time in milliseconds that has elapsed since the event was sent.
         * This field is generated by the local homeserver,
         * and may be incorrect if the local time on at least one of the two servers is out of sync,
         * which can cause the age to either be negative or greater than it actually is.
         */
        age?: number;
        /** The event that redacted this event, if any. */
        redacted_because?: RoomEvents;
        /** The client-supplied transaction ID, if the client being given the event is the same one which sent it. */
        transaction_id?: string;
    };
    /**
     * The ID of the room associated with this event.
     * Will not be present on events that arrive through `/sync`, despite being required everywhere else.
     */
    room_id: string;
}

export interface StateEvent<Type extends string = string, Content = any> extends RoomEvent<Type, Content> {
    /**
     * A unique key which defines the overwriting semantics for this piece of room state.
     * This value is often a zero-length string. The presence of this key makes this event a State Event.
     * State keys starting with an @ are reserved for referencing user IDs, such as room members.
     * With the exception of a few events, state events set with a given user's ID as the state key MUST only be set by that user.
     */
    state_key: string;
    /**
     * The previous content for this event.
     * If there is no previous content, this key will be missing.
     */
    prev_content?: Content;
}

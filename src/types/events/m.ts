// deno-lint-ignore-file camelcase

import { EncryptedFile, Event, m, RoomEvent, PushRule } from "../mod.ts";
export * as room from "./room.ts";
export * as call from "./call.ts";
export * as key from "./key/mod.ts";
export * as policy from "./policy/mod.ts";

export type identity_server = Event<
    "m.identity_server",
    {
        /** The base URL for the identity server for client-server connections. */
        base_url: string;
    }
>;

// Message Types
/** This message is the most basic message and is used to represent text. */
export type text = m.room.message<
    {
        /**
         * The format used in the `formatted_body`.
         * Currently only `org.matrix.custom.html` is supported.
         */
        format?: string;
        /**
         * The formatted version of the `body`.
         * This is required if `format` is specified.
         */
        formatted_body?: string;
    },
    "m.text"
>;

/**
 * This message is similar to `m.text` except that the sender is 'performing' the action contained in the `body` key, similar to `/me` in IRC.
 * This message should be prefixed by the name of the sender.
 * This message could also be represented in a different colour to distinguish it from regular `m.text` messages.
 */
export type emote = m.room.message<
    {
        /**
         * The format used in the `formatted_body`.
         * Currently only `org.matrix.custom.html` is supported.
         */
        format?: string;
        /**
         * The formatted version of the `body`.
         * This is required if `format` is specified.
         */
        formatted_body?: string;
    },
    "m.emote"
>;

/**
 * The `m.notice` type is primarily intended for responses from automated clients.
 * An `m.notice` message must be treated the same way as a regular `m.text` message with two exceptions.
 * Firstly, clients should present `m.notice` messages to users in a distinct manner, and secondly, `m.notice` messages must never be automatically responded to.
 * This helps to prevent infinite-loop situations where two automated clients continuously exchange messages.
 */
export type notice = m.room.message<
    {
        /**
         * The format used in the `formatted_body`.
         * Currently only `org.matrix.custom.html` is supported.
         */
        format?: string;
        /**
         * The formatted version of the `body`.
         * This is required if `format` is specified.
         */
        formatted_body?: string;
    },
    "m.notice"
>;

/** Represents a server notice for a user. */
export type server_notice = m.room.message<
    {
        /** The type of notice being represented. */
        server_notice_type: string;
        /**
         * A URI giving a contact method for the server administrator.
         * Required if the notice type is `m.server_notice.usage_limit_reached`.
         */
        admin_contact?: string;
        /**
         * The kind of usage limit the server has exceeded.
         * Required if the notice type is `m.server_notice.usage_limit_reached`.
         */
        limit_type?: string;
    },
    "m.server_notice"
>;

/** This message represents a single image and an optional thumbnail. */
export type image = m.room.message<
    {
        /**
         * A textual representation of the image.
         * This could be the alt text of the image, the filename of the image, or some kind of content description for accessibility
         * e.g. 'image attachment'.
         */
        body: string;
        /** Metadata about the image referred to in `url`. */
        info?: {
            /**
             * The intended display height of the image in pixels.
             * This may differ from the intrinsic dimensions of the image file.
             */
            h?: number;
            /**
             * The intended display width of the image in pixels.
             * This may differ from the intrinsic dimensions of the image file.
             */
            w?: number;
            /** The mimetype of the image, e.g. `image/jpeg`. */
            mimetype?: string;
            /** Size of the image in bytes. */
            size?: number;
            /** Metadata about the image referred to in `thumbnail_url`. */
            thumbnail_info?: {
                /**
                 * The intended display height of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                h?: number;
                /**
                 * The intended display width of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                w?: number;
                /** The mimetype of the image, e.g. `image/jpeg`. */
                mimetype?: string;
                /** Size of the image in bytes. */
                size?: number;
            };
        } & (
            | {
                  /**
                   * The URL (typically MXC URI) to a thumbnail of the image.
                   * Only present if the thumbnail is unencrypted.
                   */
                  thumbnail_url?: string;
              }
            | {
                  /**
                   * Information on the encrypted thumbnail file, as specified in End-to-end encryption.
                   * Only present if the thumbnail is encrypted.
                   */
                  thumbnail_file?: EncryptedFile;
              }
        );
    } & (
        | {
              /**
               * Required if the file is unencrypted.
               * The URL (typically MXC URI) to the image.
               */
              url: string;
          }
        | {
              /**
               * Required if the file is encrypted.
               * Information on the encrypted file, as specified in End-to-end encryption.
               */
              file: EncryptedFile;
          }
    ),
    "m.image"
>;

/** This message represents a generic file. */
export type file = m.room.message<
    {
        /**
         * A human-readable description of the file.
         * This is recommended to be the filename of the original upload.
         */
        body: string;
        filename?: string;
        info?: {
            mimetype?: string;
            size?: number;
            /** Metadata about the image referred to in `thumbnail_url`. */
            thumbnail_info?: {
                /**
                 * The intended display height of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                h?: number;
                /**
                 * The intended display width of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                w?: number;
                /** The mimetype of the image, e.g. `image/jpeg`. */
                mimetype?: string;
                /** Size of the image in bytes. */
                size?: number;
            };
        } & (
            | {
                  /**
                   * The URL (typically MXC URI) to a thumbnail of the image.
                   * Only present if the thumbnail is unencrypted.
                   */
                  thumbnail_url?: string;
              }
            | {
                  /**
                   * Information on the encrypted thumbnail file, as specified in End-to-end encryption.
                   * Only present if the thumbnail is encrypted.
                   */
                  thumbnail_file?: EncryptedFile;
              }
        );
    } & (
        | {
              /**
               * Required if the file is unencrypted.
               * The URL (typically MXC URI) to the file.
               */
              url: string;
          }
        | {
              /**
               * Required if the file is encrypted.
               * Information on the encrypted file, as specified in End-to-end encryption.
               */
              file: EncryptedFile;
          }
    ),
    "m.file"
>;

export type audio = m.room.message<
    {
        /**
         * A description of the audio e.g. 'Bee Gees - Stayin' Alive',
         * or some kind of content description for accessibility e.g. 'audio attachment'.
         */
        body: string;
        /** Metadata for the audio clip referred to in `url`. */
        info?: {
            duration?: number;
            mimetype?: string;
            size?: number;
        };
    } & (
        | {
              /**
               * Required if the file is unencrypted.
               * The URL (typically MXC URI) to the audio clip.
               */
              url: string;
          }
        | {
              /**
               * Required if the file is encrypted.
               * Information on the encrypted file, as specified in End-to-end encryption.
               */
              file: EncryptedFile;
          }
    ),
    "m.audio"
>;

/** This message represents a real-world location. */
export type location = m.room.message<
    {
        /**
         * A description of the location e.g. 'Big Ben, London, UK',
         * or some kind of content description for accessibility e.g. 'location attachment'.
         */
        body: string;
        /** A geo URI representing this location. */
        geo_rui: string;
        info?: {
            /** Metadata about the image referred to in `thumbnail_url`. */
            thumbnail_info?: {
                /**
                 * The intended display height of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                h?: number;
                /**
                 * The intended display width of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                w?: number;
                /** The mimetype of the image, e.g. `image/jpeg`. */
                mimetype?: string;
                /** Size of the image in bytes. */
                size?: number;
            };
        } & (
            | {
                  /**
                   * The URL (typically MXC URI) to a thumbnail of the image.
                   * Only present if the thumbnail is unencrypted.
                   */
                  thumbnail_url?: string;
              }
            | {
                  /**
                   * Information on the encrypted thumbnail file, as specified in End-to-end encryption.
                   * Only present if the thumbnail is encrypted.
                   */
                  thumbnail_file?: EncryptedFile;
              }
        );
    },
    "m.location"
>;

/** This message represents a single video clip. */
export type video = m.room.message<
    {
        /**
         * A description of the video e.g. 'Gangnam style',
         * or some kind of content description for accessibility e.g. 'video attachment'.
         */
        body: string;
        info?: {
            /** The duration of the video in milliseconds. */
            duration?: number;
            /** The height of the video in pixels. */
            h?: number;
            /** The width of the video in pixels. */
            w?: number;
            /** The mimetype of the video e.g. `video/mp4`. */
            mimetype?: string;
            /** The size of the video in bytes. */
            size?: number;
            /** Metadata about the image referred to in `thumbnail_url`. */
            thumbnail_info?: {
                /**
                 * The intended display height of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                h?: number;
                /**
                 * The intended display width of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                w?: number;
                /** The mimetype of the image, e.g. `image/jpeg`. */
                mimetype?: string;
                /** Size of the image in bytes. */
                size?: number;
            };
        } & (
            | {
                  /**
                   * The URL (typically MXC URI) to a thumbnail of the image.
                   * Only present if the thumbnail is unencrypted.
                   */
                  thumbnail_url?: string;
              }
            | {
                  /**
                   * Information on the encrypted thumbnail file, as specified in End-to-end encryption.
                   * Only present if the thumbnail is encrypted.
                   */
                  thumbnail_file?: EncryptedFile;
              }
        );
    } & (
        | {
              /**
               * Required if the file is unencrypted.
               * The URL (typically MXC URI) to the video clip.
               */
              url: string;
          }
        | {
              /**
               * Required if the file is encrypted.
               * Information on the encrypted file, as specified in End-to-end encryption.
               */
              file: EncryptedFile;
          }
    ),
    "m.video"
>;

/** This message represents a single sticker image. */
export type sticker = RoomEvent<
    "m.sticker",
    {
        /**
         * A textual representation or associated description of the sticker image.
         * This could be the alt text of the original image, or a message to accompany and further describe the sticker.
         */
        body: string;
        /** Metadata about the image referred to in `url` including a thumbnail representation. */
        info: {
            /**
             * The intended display height of the image in pixels.
             * This may differ from the intrinsic dimensions of the image file.
             */
            h?: number;
            /**
             * The intended display width of the image in pixels.
             * This may differ from the intrinsic dimensions of the image file.
             */
            w?: number;
            /** The mimetype of the image, e.g. `image/jpeg`. */
            mimetype?: string;
            /** Size of the image in bytes. */
            size?: number;
            /** Metadata about the image referred to in `thumbnail_url`. */
            thumbnail_info?: {
                /**
                 * The intended display height of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                h?: number;
                /**
                 * The intended display width of the image in pixels.
                 * This may differ from the intrinsic dimensions of the image file.
                 */
                w?: number;
                /** The mimetype of the image, e.g. `image/jpeg`. */
                mimetype?: string;
                /** Size of the image in bytes. */
                size?: number;
            };
        } & (
            | {
                  /**
                   * The URL (typically MXC URI) to a thumbnail of the image.
                   * Only present if the thumbnail is unencrypted.
                   */
                  thumbnail_url?: string;
              }
            | {
                  /**
                   * Information on the encrypted thumbnail file, as specified in End-to-end encryption.
                   * Only present if the thumbnail is encrypted.
                   */
                  thumbnail_file?: EncryptedFile;
              }
        );
        /**
         * The URL to the sticker image.
         * This must be a valid `mxc://` URI.
         */
        url: string;
    }
>;

export type typing = Event<
    "m.typing",
    {
        /** The list of user IDs typing in this room, if any. */
        user_ids: string[];
    }
> &
    Pick<RoomEvent, "room_id">;

export type receipt = Event<
    "m.receipt",
    {
        /**
         * The mapping of event ID to a collection of receipts for this event ID.
         * The event ID is the ID of the event being acknowledged and not an ID for the receipt itself.
         */
        [event_id: string]: {
            /** A collection of users who have sent `m.read` receipts for this event. */
            "m.read"?: {
                /**
                 * The mapping of user ID to receipt.
                 * The user ID is the entity who sent this receipt.
                 */
                [user_id: string]: {
                    /** The timestamp the receipt was sent at. */
                    ts?: number;
                };
            };
        };
    }
> &
    Pick<RoomEvent, "room_id">;

export type fully_read = Event<
    "m.fully_read",
    {
        /** The event the user's read marker is located at in the room. */
        event_id: string;
    }
> &
    Pick<RoomEvent, "room_id">;

export type presence = Event<
    "m.presence",
    {
        /** The current avatar URL for this user, if any. */
        avatar_url?: string;
        /** The current display name for this user, if any. */
        displayname?: string;
        /** The last time since this used performed some action, in milliseconds. */
        last_active_ago?: number;
        /** The presence state for this user */
        presence: "online" | "offline" | "unavailable";
        /** Whether the user is currently active */
        currently_active?: boolean;
        /** An optional description to accompany the presence. */
        status_msg?: string;
    }
> &
    Pick<RoomEvent, "sender">;

export type accepted_terms = Event<
    "m.accepted_terms",
    {
        /**
         * The list of URLs the user has previously accepted.
         * Should be appended to when the user agrees to new terms.
         */
        accepted?: string[];
    }
>;

export type direct = Event<
    "m.direct",
    {
        [userID: string]: string[];
    }
>;

export type dummy = Event<"m.dummy", {}>;

export type forwarded_room_key = Event<
    "m.forwarded_room_key",
    {
        /** The encryption algorithm the key in this event is to be used with. */
        algorithm: string;
        /** The room where the key is used. */
        room_id: string;
        /** The Curve25519 key of the device which initiated the session originally. */
        sender_key: string;
        /** The ID of the session that the key is for. */
        session_id: string;
        /** The key to be exchanged. */
        session_key: string;
        /**
         * The Ed25519 key of the device which initiated the session originally.
         * It is 'claimed' because the receiving device has no way to tell that the original room_key actually came from a device which owns the private part of this key unless they have done device verification.
         */
        sender_claimed_ed25519_key: string;
        /**
         * Chain of Curve25519 keys.
         * It starts out empty, but each time the key is forwarded to another device, the previous sender in the chain is added to the end of the list.
         * For example, if the key is forwarded from A to B to C, this field is empty between A and B, and contains A's Curve25519 key between B and C.
         */
        forwarding_curve25519_key_chain: string[];
    }
>;

export type ignored_user_list = Event<
    "m.ignored_user_list",
    {
        /** The map of users to ignore */
        ignored_users: {
            [userID: string]: {};
        };
    }
>;

export type push_rules = Event<
    "m.push_rules",
    {
        /** The global ruleset */
        global?: {
            content?: PushRule[];
            override?: PushRule[];
            room?: PushRule[];
            sender?: PushRule[];
            underride?: PushRule[];
        };
    }
>;

/**
 * This event type is used to exchange keys for end-to-end encryption.
 * Typically it is encrypted as an `m.room.encrypted` event, then sent as a `to-device` event.
 */
export type room_key = Event<
    "m.room_key",
    {
        /** The encryption algorithm the key in this event is to be used with. */
        algorithm: "m.megolm.v1.aes-sha2";
        /** The room where the key is used. */
        room_id: string;
        /** The ID of the session that the key is for. */
        session_id: string;
        /** The key to be exchanged. */
        session_key: string;
    }
>;

/**
 * This event type is used to request keys for end-to-end encryption.
 * It is sent as an unencrypted `to-device` event.
 */
export type room_key_request = Event<
    "m.room_key_request",
    {
        /**
         * Information about the requested key.
         * Required when `action` is `request`.
         */
        body?: {
            /** The encryption algorithm the requested key in this event is to be used with. */
            algorithm: string;
            /** The room where the key is used. */
            room_id: string;
            /** The Curve25519 key of the device which initiated the session originally. */
            sender_key: string;
            /** The ID of the session that the key is for. */
            session_id: string;
        };
        action: "request" | "request_cancellation";
        /** ID of the device requesting the key. */
        requesting_device_id: string;
        /**
         * A random string uniquely identifying the request for a key.
         * If the key is requested multiple times, it should be reused.
         * It should also reused in order to cancel a request.
         */
        request_id: string;
    }
>;

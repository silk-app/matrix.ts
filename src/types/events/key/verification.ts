import { Event } from "../../mod.ts";

/**
 * Requests a key verification with another user's devices.
 * Typically sent as a to-device event.
 */
export type request = Event<
    "m.key.verification.request",
    {
        /** The device ID which is initiating the request. */
        from_device: string;
        /**
         * An opaque identifier for the verification request.
         * Must be unique with respect to the devices involved.
         */
        transaction_id: string;
        /** The verification methods supported by the sender. */
        methods: string[];
        /**
         * The POSIX timestamp in milliseconds for when the request was made.
         * If the request is in the future by more than 5 minutes or more than 10 minutes in the past,the message should be ignored by the receiver.
         */
        timestamp: number;
    }
>;
/**
 * Begins a key verification process.
 * Typically sent as a to-device event.
 * The `method` field determines the type of verification.
 * The fields in the event will differ depending on the `method`.
 * This definition includes fields that are in common among all variants.
 */
export type start =
    | Event<
          "m.key.verification.start",
          {
              /** The device ID which is initiating the process. */
              from_device: string;
              /**
               * An opaque identifier for the verification process. Must be unique with respect to the devices involved.
               * Must be the same as the `transaction_id` given in the `m.key.verification.request` if this process is originating from a request.
               */
              transaction_id: string;
              /** The verification method to use. */
              method: string;
              /**
               * Optional method to use to verify the other user's key with.
               * Applicable when the method chosen only verifies one user's key.
               * This field will never be present if the method verifies keys both ways.
               */
              next_method?: string;
          }
      >
    | Event<
          "m.key.verification.start",
          {
              /** The device ID which is initiating the process. */
              from_device: string;
              /**
               * An opaque identifier for the verification process. Must be unique with respect to the devices involved.
               * Must be the same as the `transaction_id` given in the `m.key.verification.request` if this process is originating from a request.
               */
              transaction_id: string;
              /** The verification method to use. */
              method: "m.sas.v1";
              /**
               * The key agreement protocols the sending device understands.
               * Must include at least curve25519.
               */
              key_agreement_protocols: string[];
              /**
               * The hash methods the sending device understands.
               * Must include at least `sha256`.
               */
              hashes: string[];
              /**
               * The message authentication codes that the sending device understands.
               * Must include at least `hkdf-hmac-sha256`.
               */
              message_authentication_codes: string[];
              /**
               * The SAS methods the sending device (and the sending device's user) understands.
               * Must include at least `decimal`.
               * Optionally can include `emoji`.
               */
              short_authentication_string: ("decimal" | "emoji")[];
          }
      >;
/**
 * Cancels a key verification process/request.
 * Typically sent as a to-device event.
 */
export type cancel = Event<
    "m.key.verification.cancel",
    {
        /** The opaque identifier for the verification process/request. */
        transaction_id: string;
        /**
         * A human readable description of the code.
         * The client should only rely on this string if it does not understand the code.
         */
        reason: string;
        /**
         * The error code for why the process/request was cancelled by the user.
         * Error codes should use the Java package naming convention if not in VerificationErrorCodes
         *
         * Clients should be careful to avoid error loops.
         * For example, if a device sends an incorrect message and the client returns `m.invalid_message` to which it gets an unexpected response with `m.unexpected_message`,
         * the client should not respond again with `m.unexpected_message` to avoid the other device potentially sending another error response.
         */
        code: string;
    }
>;
/**
 * Accepts a previously sent `m.key.verification.start` message.
 * Typically sent as a to-device event.
 */
export type accept = Event<
    "m.key.verification.accept",
    {
        /**
         * An opaque identifier for the verification process.
         * Must be the same as the one used for the `m.key.verification.start` message.
         */
        transaction_id: string;
        /** The verification method to use */
        method: "m.sas.v1";
        /** The key agreement protocol the device is choosing to use, out of the options in the `m.key.verification.start` message. */
        key_agreement_protocol: string;
        /** The hash method the device is choosing to use, out of the options in the `m.key.verification.start` message. */
        hash: string;
        /** The message authentication code the device is choosing to use, out of the options in the `m.key.verification.start` message. */
        message_authentication_code: string;
        /**
         * The SAS methods both devices involved in the verification process understand.
         * Must be a subset of the options in the `m.key.verification.start` message.
         */
        short_authentication_string: ("decimal" | "emoji")[];
        /** The hash (encoded as unpadded base64) of the concatenation of the device's ephemeral public key (encoded as unpadded base64) and the canonical JSON representation of the `m.key.verification.start` message. */
        commitment: string;
    }
>;
/**
 * Sends the ephemeral public key for a device to the partner device.
 * Typically sent as a to-device event.
 */
export type key = Event<
    "m.key.verification.key",
    {
        /**
         * An opaque identifier for the verification process.
         * Must be the same as the one used for the `m.key.verification.start` message.
         */
        transaction_id: string;
        /** The device's ephemeral public key, encoded as unpadded base64. */
        key: string;
    }
>;
/**
 * Sends the MAC of a device's key to the partner device.
 * Typically sent as a to-device event.
 */
export type mac = Event<
    "m.key.verification.mac",
    {
        /**
         * An opaque identifier for the verification process.
         * Must be the same as the one used for the `m.key.verification.start` message.
         */
        transaction_id: string;
        /**
         * A map of the key ID to the MAC of the key, using the algorithm in the verification process.
         * The MAC is encoded as unpadded base64.
         */
        mac: {
            [keyID: string]: string;
        };
        /** The MAC of the comma-separated, sorted, list of key IDs given in the `mac` property, encoded as unpadded base64. */
        keys: string;
    }
>;

import { StateEvent } from "../../mod.ts";

export type room = StateEvent<
    "m.policy.rule.room",
    {
        /**
         * The entity affected by this rule.
         * Glob characters `*` and `?` can be used to match zero or more and one or more characters respectively.
         */
        entity: string;
        /** The suggested action to take. Currently only `m.ban` is specified. */
        recommendation: string;
        /** The human-readable description for the `recommendation`. */
        reason: string;
    }
> & {
    /** An arbitrary string decided upon by the sender. */
    state_key: string;
};

export type server = StateEvent<
    "m.policy.rule.room",
    {
        /**
         * The entity affected by this rule.
         * Glob characters `*` and `?` can be used to match zero or more and one or more characters respectively.
         */
        entity: string;
        /** The suggested action to take. Currently only `m.ban` is specified. */
        recommendation: string;
        /** The human-readable description for the `recommendation`. */
        reason: string;
    }
> & {
    /** An arbitrary string decided upon by the sender. */
    state_key: string;
};

export type user = StateEvent<
    "m.policy.rule.room",
    {
        /**
         * The entity affected by this rule.
         * Glob characters `*` and `?` can be used to match zero or more and one or more characters respectively.
         */
        entity: string;
        /** The suggested action to take. Currently only `m.ban` is specified. */
        recommendation: string;
        /** The human-readable description for the `recommendation`. */
        reason: string;
    }
> & {
    /** An arbitrary string decided upon by the sender. */
    state_key: string;
};

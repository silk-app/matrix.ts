import { RoomEvent } from "../mod.ts";

/** This event is sent by the caller when they wish to establish a call. */
export type invite = RoomEvent<
    "m.call.invite",
    {
        /** A unique identifier for the call. */
        call_id: string;
        /** The session description object */
        offer: {
            /** The type of session description. */
            type: "offer";
            /** The SDP text of the session description. */
            sdp: string;
        };
        /** The version of the VoIP specification this message adheres to. This specification is version 0. */
        version: number;
        /**
         * The time in milliseconds that the invite is valid for. Once the invite age exceeds this value, clients should discard it.
         * They should also no longer show the call as awaiting an answer in the UI.
         */
        lifetime: number;
    }
>;

/**
 * This event is sent by callers after sending an invite and by the callee after answering.
 * Its purpose is to give the other party additional ICE candidates to try using to communicate.
 */
export type candidates = RoomEvent<
    "m.call.candidates",
    {
        /** The ID of the call this event relates to. */
        call_id: string;
        /** Array of objects describing the candidates. */
        candidates: {
            /** The SDP media type this candidate is intended for. */
            sdpMid: string;
            /** The index of the SDP 'm' line this candidate is intended for. */
            sdpMLineIndex: number;
            /** The SDP 'a' line of the candidate. */
            candidate: string;
        }[];
        /** The version of the VoIP specification this messages adheres to. This specification is version 0. */
        version: number;
    }
>;

/** This event is sent by the callee when they wish to answer the call. */
export type answer = RoomEvent<
    "m.call.answer",
    {
        /** The ID of the call this event relates to. */
        call_id: string;
        /** The session description object */
        answer: {
            /** The type of session description. */
            type: "answer";
            /** The SDP text of the session description. */
            sdp: string;
        };
        version: number;
    }
>;

/**
 * Sent by either party to signal their termination of the call.
 * This can be sent either once the call has has been established or before to abort the call.
 */
export type hangup = RoomEvent<
    "m.call.hangup",
    {
        /** The ID of the call this event relates to. */
        call_id: string;
        /**
         * The version of the VoIP specification this message adheres to.
         * This specification is version 0.
         */
        version: number;
        /**
         * Optional error reason for the hangup.
         * This should not be provided when the user naturally ends or rejects the call.
         * When there was an error in the call negotiation, this should be ice_failed for when ICE negotiation fails or invite_timeout for when the other party did not answer in time.
         */
        reason?: "ice_failed" | "invite_timeout";
    }
>;

// deno-lint-ignore-file camelcase

import { m, Event, RoomEvent, RoomStateEvents, Signatures, StateEvent } from "../mod.ts";

/**
 * This event is used to inform the room about which alias should be considered the canonical one, and which other aliases point to the room.
 * This could be for display purposes or as suggestion to users which alias to use to advertise and access the room.
 */
export type canonical_alias = StateEvent<
    "m.room.canonical_alias",
    {
        /**
         * The canonical alias for the room.
         * If not present, null, or empty the room should be considered to have no canonical alias.
         */
        alias?: string;
        /**
         * Alternative aliases the room advertises.
         * This list can have aliases despite the `alias` field being null, empty, or otherwise not present.
         */
        alt_aliases?: string[];
    }
> & {
    state_key: "";
};

/**
 * This is the first event in a room and cannot be changed.
 * It acts as the root of all other events.
 */
export type create = StateEvent<
    "m.room.create",
    {
        /**
         * The `user_id` of the room creator.
         * This is set by the homeserver.
         */
        creator: string;
        /**
         * Whether users on other servers can join this room.
         * Defaults to `true` if key does not exist.
         */
        "m.federate"?: boolean;
        /**
         * The version of the room.
         * Defaults to `"1"` if the key does not exist.
         */
        room_version?: string;
        /** A reference to the room this room replaces, if the previous room was upgraded. */
        predecessor?: {
            /** The ID of the old room. */
            room_id: string;
            /** The event ID of the last known event in the old room. */
            event_id: string;
        };
    }
> & {
    state_key: "";
};

/**
 * A room may be `public` meaning anyone can join the room without any prior action.
 * Alternatively, it can be `invite` meaning that a user who wishes to join the room must first receive an invite to the room from someone already inside of the room.
 * Currently, `knock` and `private` are reserved keywords which are not implemented.
 */
export type join_rules = StateEvent<
    "m.room.join_rules",
    {
        /**  The type of rules used for users wishing to join this room. */
        join_rule: "public" | "knock" | "invite" | "private";
    }
> & {
    state_key: "";
};

/**
 * Adjusts the membership state for a user in a room.
 * It is preferable to use the membership APIs (`/rooms/<room id>/invite` etc) when performing membership actions rather than adjusting the state directly as there are a restricted set of valid transformations.
 * For example, user A cannot force user B to join a room, and trying to force this state change directly will fail.
 *
 * The following membership states are specified:
 * - `invite` - The user has been invited to join a room, but has not yet joined it. They may not participate in the room until they join.
 * - `join` - The user has joined the room (possibly after accepting an invite), and may participate in it.
 * - `leave` - The user was once joined to the room, but has since left (possibly by choice, or possibly by being kicked).
 * - `ban` - The user has been banned from the room, and is no longer allowed to join it until they are un-banned from the room (by having their membership state set to a value other than ban).
 * - `knock` - This is a reserved word, which currently has no meaning.
 *
 * The `third_party_invite` property will be set if this invite is an `invite` event and is the successor of an `m.room.third_party_invite` event, and absent otherwise.
 *
 * This event may also include an `invite_room_state` key inside the event's `unsigned` data.
 * If present, this contains an array of `StrippedState` Events.
 * These events provide information on a subset of state events such as the room name.
 *
 * The user for which a membership applies is represented by the `state_key`.
 * Under some conditions, the `sender` and `state_key` may not match - this may be interpreted as the `sender` affecting the membership state of the `state_key` user.
 *
 * The `membership` for a given user can change over time.
 * The table below represents the various changes over time and how clients and servers must interpret those changes.
 * Previous membership can be retrieved from the `prev_content` object on an event.
 * If not present, the user's previous membership must be assumed as `leave`.
 *
 * | | **to `invite`** | **to `join`** | **to `leave`** | **to `ban`** | **to `knock`** |
 * |:- |:- |:- |:- |:- |:- |
 * | **from `invite`** | No change. | User joined the room. | If the `state_key` is the same as the `sender`, the user rejected the invite. Otherwise, the `state_key` user had their invite revoked. | User was banned. | Not implemented. |
 * | **from `join`** | Must never happen. | `displayname` or `avatar_url` changed. | If the `state_key` is the same as the `sender`, the user left. Otherwise, the `state_key` user was kicked. | User was kicked and banned. | Not implemented. |
 * | **from `leave`** | New invitation sent. | User joined. | No change. | User was banned. | Not implemented. |
 * | **from `ban`** | Must never happen. | Must never happen. | User was unbanned. | No change. | Not implemented. |
 * | **from `knock`** | Not implemented. | Not implemented. | Not implemented. | Not implemented. | Not implemented. |
 */
export type member = StateEvent<
    "m.room.member",
    {
        /** The avatar URL for this user, if any. */
        avatar_url?: string;
        /** The display name for this user, if any. */
        displayname?: string | null;
        /** The membership state of the user. */
        membership: "invite" | "join" | "knock" | "leave" | "ban";
        /**
         * Flag indicating if the room containing this event was created with the intention of being a direct chat.
         * See [Direct Messaging](https://matrix.org/docs/spec/client_server/r0.6.1#module-dm).
         */
        is_direct?: boolean;
        third_party_invite?: {
            /** A name which can be displayed to represent the user instead of their third party identifier */
            display_name: string;
            /** A block of content which has been signed, which servers can use to verify the event. Clients should ignore this. */
            signed: {
                /** The invited matrix user ID. Must be equal to the user_id property of the event. */
                mxid: string;
                /** A single signature from the verifying server, in the format specified by the Signing Events section of the server-server API. */
                signatures: Signatures;
                /** The token property of the containing third_party_invite object. */
                token: string;
            };
        };
        /** Contains optional extra information about the event. */
        unsigned?: {
            /**
             * A subset of the state of the room at the time of the invite, if `membership` is `invite`.
             * Note that this state is informational, and SHOULD NOT be trusted;
             * once the client has joined the room, it SHOULD fetch the live state from the server and discard the invite_room_state.
             * Also, clients must not rely on any particular state being present here;
             * they SHOULD behave properly (with possibly a degraded but not a broken experience) in the absence of any particular events here.
             * If they are set on the room, at least the state for `m.room.avatar`, `m.room.canonical_alias`, `m.room.join_rules`, and `m.room.name` SHOULD be included.
             */
            invite_room_state?: Pick<RoomStateEvents, "content" | "state_key" | "type" | "sender">[];
        };
    }
> & {
    /**
     * The `user_id` this membership event relates to.
     * In all cases except for when `membership` is `join`, the user ID sending the event does not need to match the user ID in the `state_key`,` unlike other events.
     * Regular authorisation rules still apply.
     */
    state_key: string;
};

/**
 * This event specifies the minimum level a user must have in order to perform a certain action.
 * It also specifies the levels of each user in the room.
 *
 * If a `user_id` is in the `users` list, then that `user_id` has the associated power level.
 * Otherwise they have the default level `users_default`.
 * If `users_default` is not supplied, it is assumed to be 0.
 * If the room contains no `m.room.power_levels` event, the room's creator has a power level of 100, and all other users have a power level of 0.
 *
 * The level required to send a certain event is governed by `events`, `state_default` and `events_default`.
 * If an event type is specified in `events`, then the user must have at least the level specified in order to send that event.
 * If the event type is not supplied, it defaults to `events_default` for Message Events and `state_default` for State Events.
 *
 * If there is no `state_default` in the `m.room.power_levels` event, the `state_default` is 50.
 * If there is no `events_default` in the `m.room.power_levels` event, the `events_default` is 0.
 * If the room contains no `m.room.power_levels` event, both the `state_default` and `events_default` are 0.
 *
 * The power level required to invite a user to the room, kick a user from the room, ban a user from the room, or redact an event, is defined by `invite`, `kick`, `ban`, and `redact`, respectively.
 * Each of these levels defaults to 50 if they are not specified in the `m.room.power_levels` event, or if the room contains no `m.room.power_levels` event.
 *
 * > ### Note
 * > As noted above, in the absence of an `m.room.power_levels` event, the `state_default` is 0, and all users are considered to have power level 0.
 * > That means that **any** member of the room can send an `m.room.power_levels` event, changing the permissions in the room.
 * > Server implementations should therefore ensure that each room has an `m.room.power_levels` event as soon as it is created.
 * > See also the documentation of the `/createRoom` API.
 */
export type power_levels = StateEvent<
    "m.room.power_levels",
    {
        /** The level required to ban a user. Defaults to 50 if unspecified. */
        ban?: number;
        /**
         * The level required to send specific event types.
         * This is a mapping from event type to power level required.
         */
        events?: {
            [event in RoomStateEvents["type"]]?: number;
        };
        /**
         * The default level required to send message events.
         * Can be overridden by the `events` key.
         * Defaults to 0 if unspecified.
         */
        events_default?: number;
        /**
         * The level required to invite a user.
         * Defaults to 50 if unspecified.
         */
        invite?: number;
        /**
         * The level required to kick a user.
         * Defaults to 50 if unspecified.
         */
        kick?: number;
        /**
         * The level required to redact an event.
         * Defaults to 50 if unspecified.
         */
        redact?: number;
        /**
         * The default level required to send state events.
         * Can be overridden by the `events` key.
         * Defaults to 50 if unspecified.
         */
        state_default?: number;
        /**
         * The power levels for specific users.
         * This is a mapping from `user_id` to power level for that user.
         */
        users?: {
            [user_id: string]: number;
        };
        /**
         * The default power level for every user in the room, unless their `user_id` is mentioned in the `users` key.
         * Defaults to 0 if unspecified.
         */
        users_default?: number;
        /**
         * The power level requirements for specific notification types.
         * This is a mapping from `key` to power level for that notifications key.
         */
        notifications?: {
            room?: number;
        };
    }
> & {
    state_key: "";
};

/**
 * Events can be redacted by either room or server admins.
 * Redacting an event means that all keys not required by the protocol are stripped off, allowing admins to remove offensive or illegal content that may have been attached to any event.
 * This cannot be undone, allowing server owners to physically delete the offending data.
 * There is also a concept of a moderator hiding a message event, which can be undone, but cannot be applied to state events.
 * The event that has been redacted is specified in the `redacts` event level key.
 */
export type redaction = RoomEvent<
    "m.room.redaction",
    {
        /** The reason for the redaction, if any. */
        reason?: string;
    }
>;

/**
 * This event is used when sending messages in a room. Messages are not limited to be text.
 * The msgtype key outlines the type of message, e.g. text, audio, image, video, etc.
 * The body key is text and MUST be used with every kind of msgtype as a fallback mechanism for when a client cannot render a message.
 * This allows clients to display something even if it is just plain text.
 * For more information on msgtypes, see m.room.message msgtypes.
 */
export type message<Content, Type extends string = string> = RoomEvent<
    "m.room.message",
    {
        /** The textual representation of this message. */
        body: string;
        /** The type of message, e.g. `m.image`, `m.text` */
        msgtype: Type;
        "m.relates_to"?: {
            /**
             * A rich reply is formed through use of an `m.relates_to` relation for `m.in_reply_to` where a single key, `event_id`, is used to reference the event being replied to.
             * The referenced event ID SHOULD belong to the same room where the reply is being sent.
             * Clients should be cautious of the event ID belonging to another room, or being invalid entirely.
             * Rich replies can only be constructed in the form of `m.room.message` events with a `msgtype` of `m.text` or `m.notice`.
             * Due to the fallback requirements, rich replies cannot be constructed for types of `m.emote`, `m.file`, etc.
             * Rich replies may reference any other `m.room.message` event, however.
             * Rich replies may reference another event which also has a rich reply, infinitely.
             */
            "m.in_reply_to"?: {
                event_id: string;
            };
        };
    } & Content
>;

/**
 * This event type is used when sending encrypted events.
 * It can be used either within a room (in which case it will have all of the Room Event fields), or as a to-device event.
 */
export type encrypted = Partial<RoomEvent> &
    Event<
        "m.room.encrypted",
        {
            /**
             * The encryption algorithm used to encrypt this event.
             * The value of this field determines which other properties will be present
             */
            algorithm: "m.olm.v1.curve25519-aes-sha2" | "m.megolm.v1.aes-sha2";
            /**
             * The encrypted content of the event.
             * Either the encrypted payload itself, in the case of a Megolm event, or a map from the recipient Curve25519 identity key to ciphertext information, in the case of an Olm event.
             * For more details, see `Messaging Algorithms`.
             */
            ciphertext:
                | string
                | {
                      [recipientIdentityKey: string]: {
                          /** The encrypted payload. */
                          body?: string;
                          /** The Olm message type. */
                          type?: number;
                      };
                  };
            /** The Curve25519 key of the sender. */
            sender_key: string;
            /** The ID of the sending device. Required with Megolm. */
            device_id?: string;
            /** The ID of the session used to encrypt the message. Required with Megolm. */
            session_id?: string;
        }
    >;

export type encryption = StateEvent<
    "m.room.encryption",
    {
        /** The encryption algorithm to be used to encrypt messages sent in this room. */
        algorithm: "m.megolm.v1.aes-sha2";
        /**
         * How long the session should be used before changing it.
         * `604800000` (a week) is the recommended default.
         */
        rotation_period_ms?: number;
        /**
         * How many messages should be sent before changing the session.
         * `100` is the recommended default.
         */
        rotation_period_msgs?: number;
    }
> & {
    state_key: "";
};

/**
 * This event controls whether guest users are allowed to join rooms.
 * If this event is absent, servers should act as if it is present and has the guest_access value "forbidden".
 */
export type guest_access = StateEvent<
    "m.room.guest_access",
    {
        /** Whether guests can join the room. */
        guest_access: "can_join" | "forbidden";
    }
> & {
    state_key: "";
};

/** This event controls whether a user can see the events that happened in a room from before they joined. */
export type history_visibility = StateEvent<
    "m.room.history_visibility",
    {
        /** Who can see the room history. */
        history_visibility: "invited" | "joined" | "shared" | "world_readable";
    }
> & {
    state_key: "";
};

/**
 * A room has an opaque room ID which is not human-friendly to read.
 * A room alias is human-friendly, but not all rooms have room aliases.
 * The room name is a human-friendly string designed to be displayed to the end-user.
 * The room name is not unique, as multiple rooms can have the same room name set.
 *
 * A room with an `m.room.name` event with an absent, null, or empty `name` field should be treated the same as a room with no `m.room.name` event.
 *
 * An event of this type is automatically created when creating a room using `/createRoom` with the `name` key.
 */
export type name = StateEvent<
    "m.room.name",
    {
        /** The name of the room. This MUST NOT exceed 255 bytes. */
        name: string;
    }
> & {
    state_key: "";
};

/**
 * This event is used to "pin" particular events in a room for other participants to review later.
 * The order of the pinned events is guaranteed and based upon the order supplied in the event.
 * Clients should be aware that the current user may not be able to see some of the events pinned due to visibility settings in the room.
 * Clients are responsible for determining if a particular event in the pinned list is displayable, and have the option to not display it if it cannot be pinned in the client.
 */
export type pinned_events = StateEvent<
    "m.room.pinned_events",
    {
        /** An ordered list of event IDs to pin. */
        pinned: string[];
    }
> & {
    state_key: "";
};

/**
 * An event to indicate which servers are permitted to participate in the room.
 * Server ACLs may allow or deny groups of hosts.
 * All servers participating in the room, including those that are denied, are expected to uphold the server ACL. Servers that do not uphold the ACLs MUST be added to the denied hosts list in order for the ACLs to remain effective.
 *
 * The `allow` and `deny` lists are lists of globs supporting `?` and `*` as wildcards.
 * When comparing against the server ACLs, the suspect server's port number must not be considered.
 * Therefore `evil.com`, `evil.com:8448`, and `evil.com:1234` would all match rules that apply to `evil.com`, for example.
 *
 * The ACLs are applied to servers when they make requests, and are applied in the following order:
 * 1. If there is no `m.room.server_acl` event in the room state, allow.
 * 2. If the server name is an IP address (v4 or v6) literal, and `allow_ip_literals` is present and `false`, deny.
 * 3. If the server name matches an entry in the `deny` list, deny.
 * 4. If the server name matches an entry in the `allow` list, allow.
 * 5. Otherwise, deny.
 *
 * > **Note:** Server ACLs do not restrict the events relative to the room DAG via authorisation rules, but instead act purely at the network layer to determine which servers are allowed to connect and interact with a given room.
 *
 * > **Warning:** Failing to provide an `allow` rule of some kind will prevent **all** servers from participating in the room, including the sender.
 * > This renders the room unusable.
 * > A common allow rule is `[ "*" ]` which would still permit the use of the `deny` list without losing the room.
 *
 * > **Warning:** All compliant servers must implement server ACLs.
 * > However, legacy or noncompliant servers exist which do not uphold ACLs, and these MUST be manually appended to the denied hosts list when setting an ACL to prevent them from leaking events from banned servers into a room.
 * > Currently, the only way to determine noncompliant hosts is to check the `prev_events` of leaked events, therefore detecting servers which are not upholding the ACLs.
 * > Server versions can also be used to try to detect hosts that will not uphold the ACLs, although this is not comprehensive.
 * > Server ACLs were added in Synapse v0.32.0, although other server implementations and versions exist in the world.
 */
export type server_acl = StateEvent<
    "m.room.server_acl",
    {
        /**
         * True to allow server names that are IP address literals.
         * False to deny.
         * Defaults to true if missing or otherwise not a boolean.
         *
         * This is strongly recommended to be set to `false` as servers running with IP literal names are strongly discouraged in order to require legitimate homeservers to be backed by a valid registered domain name.
         */
        allow_ip_literals?: boolean;
        /**
         * The server names to allow in the room, excluding any port information.
         * Wildcards may be used to cover a wider range of hosts, where `*` matches zero or more characters and `?` matches exactly one character.
         *
         * **This defaults to an empty list when not provided, effectively disallowing every server.**
         */
        allow?: string[];
        /**
         * The server names to disallow in the room, excluding any port information.
         * Wildcards may be used to cover a wider range of hosts, where `*` matches zero or more characters and `?` matches exactly one character.
         *
         * This defaults to an empty list when not provided.
         */
        deny?: string[];
    }
> & {
    state_key: "";
};

/**
 * Acts as an `m.room.member` invite event, where there isn't a target user_id to invite.
 * This event contains a token and a public key whose private key must be used to sign the token.
 * Any user who can present that signature may use this invitation to join the target room.
 */
export type third_party_invite = StateEvent<
    "m.room.third_party_invite",
    {
        /**
         * A user-readable string which represents the user who has been invited.
         * This should not contain the user's third party ID, as otherwise when the invite is accepted it would leak the association between the matrix ID and the third party ID.
         */
        display_name: string;
        /**
         * A URL which can be fetched, with querystring public_key=public_key, to validate whether the key has been revoked.
         * The URL must return a JSON object containing a boolean property named 'valid'.
         */
        key_validity_url: string;
        /**
         * A base64-encoded ed25519 key with which token must be signed (though a signature from any entry in public_keys is also sufficient).
         * This exists for backwards compatibility.
         */
        public_key: string;
        /** Keys with which the token may be signed. */
        public_keys?: {
            /**
             * An optional URL which can be fetched, with querystring public_key=public_key, to validate whether the key has been revoked.
             * The URL must return a JSON object containing a boolean property named 'valid'.
             * If this URL is absent, the key must be considered valid indefinitely.
             */
            key_validity_url?: string;
            /** A base-64 encoded ed25519 key with which token may be signed. */
            public_key: string;
        }[];
    }
> & {
    /** The token, of which a signature must be produced in order to join the room. */
    state_key: string;
};

/** A state event signifying that a room has been upgraded to a different room version, and that clients should go there. */
export type tombstone = StateEvent<
    "m.room.tombstone",
    {
        /** A server-defined message. */
        body: string;
        /** The new room the client should be visiting. */
        replacement_room: string;
    }
> & {
    state_key: "";
};

/**
 * A topic is a short message detailing what is currently being discussed in the room.
 * It can also be used as a way to display extra information about the room, which may not be suitable for the room name.
 * The room topic can also be set when creating a room using `/createRoom` with the `topic` key.
 */
export type topic = StateEvent<
    "m.room.topic",
    {
        /** The topic text. */
        topic: string;
    }
> & {
    state_key: "";
};

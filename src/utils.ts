import { RateLimitError, RequestError } from "./errors.ts";
import { APIResponse, FileAPIResponse } from "./types/mod.ts";

/** Generate a RequestInit
 *
 * @param method The HTTP method to use
 * @param token The authorization token
 * @param body The request JSON body
 * @param contentType The request content type (mimetype)
 */
// deno-lint-ignore no-explicit-any
export function generateRequestInit(method: string, token?: string, body?: any, contentType: string | undefined = "application/json"): RequestInit {
    const headers = new Headers();
    if (token) headers.set("authorization", token);
    if (contentType) headers.set("content-type", contentType);
    return {
        method,
        headers,
        body: body instanceof Blob ? body : JSON.stringify(body)
    };
}

/** Generate an APIResponse from a Response
 *
 * @param response The response to use
 */
export async function generateAPIResponse<T>(response: Response): Promise<APIResponse<T>> {
    try {
        const out: APIResponse<T> = await response.json();
        out._response = response;
        return out;
    } catch (error) {
        if (error.name === "SyntaxError") {
            // deno-lint-ignore no-explicit-any
            return <any>{
                _response: response
            };
        } else {
            throw error;
        }
    }
}

/** Generate a URL with query parameters
 *
 * @param url The base URL
 * @param parameters The query parameters
 */
export function generateQueriedURL(
    url: string,
    parameters: {
        [key: string]: Parameters<typeof encodeURIComponent>[0] | Parameters<typeof encodeURIComponent>[0][] | undefined;
    }
): string {
    const searchParams = new URLSearchParams();
    Object.entries(parameters).forEach(([key, value]) => {
        if (value !== undefined) {
            if (Array.isArray(value)) {
                value.forEach(subValue => searchParams.append(key, encodeURIComponent(subValue)));
            } else {
                searchParams.append(key, encodeURIComponent(value));
            }
        }
    });
    return url + "?" + searchParams.toString();
}

/** Generate a file API response from a response
 *
 * @param response The response to use
 */
export async function generateFileAPIResponse(response: Response): Promise<FileAPIResponse> {
    const blob = await response.blob();
    const filename = response.headers.get("content-disposition")?.split("filename=")[1];
    const fileEnding = blob.type.split("/")[1];
    const file = new File([blob], filename || (fileEnding ? "unknown." + fileEnding : "unknown"));
    return { file, response };
}

/** Handle a rate limit response
 *
 * @throws A RequestError containing a M_LIMIT_EXCEEDED error
 *
 * @param response The request's original response
 * @param retryFunction The function to call on a retry
 * @param args The function's arguments - `retryDelay` and `maxRetries` must be manually set to 0
 * @param retryDelay The number of milliseconds to wait between retries if server does not supply a value
 * @param maxRetries The maximum number of retries
 */
// deno-lint-ignore no-explicit-any
export async function handleRateLimitRetry<T extends (...args: any[]) => any>(
    response: Response,
    retryFunction: T,
    args: Parameters<T>,
    retryDelay: number,
    maxRetries: number
): Promise<ReturnType<T>> {
    let errorData: RateLimitError = await response.json();
    for (let retry = 0; retry < maxRetries; retry++) {
        const doRetry: Promise<ReturnType<T>> = new Promise((resolve, reject) => {
            setTimeout(async () => {
                const result = await retryFunction(...args);
                // deno-lint-ignore no-explicit-any
                if (<any>result instanceof RequestError && result?.errorData && result.errorData.errcode === "M_LIMIT_EXCEEDED") {
                    errorData = result.errorData;
                    reject(result);
                } else {
                    resolve(result);
                }
            }, errorData.retry_after_ms || retryDelay);
        });
        try {
            return await doRetry;
        } catch (result) {
            if (retry + 1 === maxRetries) {
                throw new RequestError(result?.url, response, (<RequestError>result).requestInit, "This request was rate-limited.", errorData);
            }
        }
    }
    // Quite dodgy, might want to improve this later
    throw new RequestError("undefined", Response.prototype, undefined, "Ran out of retries for rate limit");
}

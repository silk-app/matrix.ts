// deno-lint-ignore-file camelcase

import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, errorMessages } from "../constants.ts";
import { APIError, RequestError } from "../errors.ts";
import { APIResponse, Events, PushRule } from "../types/mod.ts";
import { generateAPIResponse, generateQueriedURL, generateRequestInit, handleRateLimitRetry } from "../utils.ts";
import { join } from "../deps.ts";
import { endpoints } from "../constants.ts";

/** ## Get Notifications
 * Gets a list of events that the user has been notified about.
 *
 * ### Implementation Notes
 * This API is used paginate through the list of events that the user has been, or would have been notified about.
 *
 * @param from Pagination token given to retrieve the next set of events.
 * @param limit Limit on the number of events to return in this request.
 * @param only Allows basic filtering of events returned. Supply `highlight` to return only events where the notification had the highlight tweak set.
 * @returns A batch of events is being returned
 */
export async function getNotifications(
    token: string,
    from?: string,
    limit?: number,
    only?: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          next_token?: string;
          notifications: {
              /** The action(s) to perform when the conditions for this rule are met. See `Push Rules: API`_. */
              // deno-lint-ignore no-explicit-any (Type not documented)
              actions: any[];
              /** The Event object for the event that triggered the notification. */
              // The prev_content shuffle seems weird, but that is how it is documented
              event: Events;
              /** The profile tag of the rule that matched this event. */
              profile_tag?: string;
              /** Indicates whether the user has sent a read receipt indicating that they have read this message. */
              read: boolean;
              /** The ID of the room in which the event was posted. */
              room_id: string;
              /** The unix timestamp at which the event notification was sent, in milliseconds. */
              ts: number;
          }[];
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.notifications), { from, limit, only });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Pushers
 * Gets the current pushers for the authenticated user.
 *
 * ### Implementation Notes
 * Gets all currently active pushers for the authenticated user.
 *
 * @returns The pushers for this user.
 */
export async function getPushers(
    token: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** An array containing the current pushers for the user */
          pushers?: {
              /**  A string that will allow the user to identify what application owns this pusher. */
              app_display_name: string;
              /** This is a reverse-DNS style identifier for the application. Max length, 64 chars. */
              app_id: string;
              /** A dictionary of information for the pusher implementation itself. */
              data: {
                  /** The format to use when sending notifications to the Push Gateway. */
                  format?: string;
                  /** Required if `kind` is `http`. The URL to use to send notifications to. */
                  url?: string;
              };
              /** A string that will allow the user to identify what device owns this pusher. */
              device_display_name: string;
              /** The kind of pusher. `"http"` is a pusher that sends HTTP pokes. */
              kind: string;
              /** The preferred language for receiving notifications (e.g. 'en' or 'en-US') */
              lang: string;
              /** This string determines which set of device specific rules this pusher executes. */
              profile_tag?: string;
              /** This is a unique identifier for this pusher. See `/set` for more detail. Max length, 512 bytes. */
              pushkey: string;
          }[];
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.pushers);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Post Pusher
 * Modify a pusher for this user on the homeserver.
 *
 * ### Implementation Notes
 * This endpoint allows the creation, modification and deletion of `pushers`_ for this user ID.
 * The behaviour of this endpoint varies depending on the values in the JSON body.
 *
 * @returns The pusher was set.
 */
export async function postPusher(
    token: string,
    parameters: {
        /** A string that will allow the user to identify what application owns this pusher. */
        app_display_name: string;
        /**
         * This is a reverse-DNS style identifier for the application.
         * It is recommended that this end with the platform, such that different platform versions get different app identifiers.
         * Max length, 64 chars.If the `kind` is `"email"`, this is `"m.email"`.
         */
        app_id: string;
        /**
         * If true, the homeserver should add another pusher with the given pushkey and App ID in addition to any others with different user IDs.
         * Otherwise, the homeserver must remove any other pushers with the same App ID and pushkey for different users.
         * The default is `false`.
         */
        append?: boolean;
        /**
         * A dictionary of information for the pusher implementation itself.
         * If `kind` is `http`, this should contain `url` which is the URL to use to send notifications to.
         */
        data: {
            /**
             * The format to send notifications in to Push Gateways if the `kind` is `http`.
             * The details about what fields the homeserver should send to the push gateway are defined in the `Push Gateway Specification`_.
             * Currently the only format available is 'event_id_only'.
             */
            format?: string;
            /**
             * Required if `kind` is `http`.
             * The URL to use to send notifications to.
             * MUST be an HTTPS URL with a path of `/_matrix/push/v1/notify`.
             */
            url?: string;
        };
        device_display_name: string;
        /**
         * The kind of pusher to configure.
         * `"http"` makes a pusher that sends HTTP pokes.
         * `"email"` makes a pusher that emails the user with unread notifications.
         * `null` deletes the pusher.
         */
        kind: string;
        /** The preferred language for receiving notifications (e.g. 'en' or 'en-US'). */
        lang: string;
        /** This string determines which set of device specific rules this pusher executes. */
        profile_tag?: string;
        /**
         * This is a unique identifier for this pusher.
         * The value you should use for this is the routing or destination address information for the notification, for example, the APNS token for APNS or the Registration ID for GCM.
         * If your notification client has no such concept, use any unique identifier.
         * Max length, 512 bytes.If the `kind` is `"email"`, this is the email address to send notifications to.
         */
        pushkey: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<APIError>> {
    const url = join(baseURL, endpoints.setPusher);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(url, response, requestInit, "One or more of the pusher values were invalid.", await response.json());
        case 429:
            try {
                return await handleRateLimitRetry(response, postPusher, [token, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Push Rules
 * Retrieve all push rulesets.
 *
 * ### Implementation Notes
 * Retrieve all push rulesets for this user.
 * Clients can "drill-down" on the rulesets by suffixing a `scope` to this path e.g. `/pushrules/global/`.
 * This will return a subset of this data under the specified key e.g. the `global` key.
 *
 * @returns All the push rulesets for this user.
 */
export async function getPushRules(
    token: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          global: {
              content?: PushRule[];
              override?: PushRule[];
              room?: PushRule[];
              sender?: PushRule[];
              underride?: PushRule[];
          };
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.pushRules);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Delete Push Rule
 * Delete a push rule.
 *
 * ### Implementation Notes
 * This endpoint removes the push rule defined in the path.
 *
 * @param scope `global` to specify global rules.
 * @param kind The kind of rule.
 * @param ruleID The identifier for the rule.
 * @returns The push rule was deleted.
 */
export async function deletePushRule(
    token: string,
    scope: string,
    kind: "content" | "room" | "sender" | "override" | "underride",
    ruleID: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.pushRule(scope, kind, ruleID));
    const requestInit = generateRequestInit("DELETE", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Push Rule
 * Retrieve a push rule.
 *
 * ### Implementation Notes
 * Retrieve a single specified push rule.
 *
 * @param scope `global` to specify global rules.
 * @param kind The kind of rule.
 * @param ruleID The identifier for the rule.
 * @returns The specific push rule. This will also include keys specific to the rule itself such as the rule's `actions` and `conditions` if set.
 */
export async function getPushRule(
    token: string,
    scope: string,
    kind: "content" | "room" | "sender" | "override" | "underride",
    ruleID: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<PushRule> | RequestError> {
    const url = join(baseURL, endpoints.pushRule(scope, kind, ruleID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Push Rule
 * Add or change a push rule.
 *
 * ## Implementation Notes
 * This endpoint allows the creation, modification and deletion of pushers for this user ID.
 * The behaviour of this endpoint varies depending on the values in the JSON body.
 *
 * When creating push rules, they MUST be enabled by default.
 *
 * @param scope `global` to specify global rules.
 * @param kind The kind of rule.
 * @param ruleID The identifier for the rule.
 * @param before Use 'before' with a `rule_id` as its value to make the new rule the next-most important rule with respect to the given user defined rule. It is not possible to add a rule relative to a predefined server rule.
 * @param after This makes the new rule the next-less important rule relative to the given user defined rule. It is not possible to add a rule relative to a predefined server rule.
 * @returns The push rule was created/updated.
 */
export async function setPushRule(
    token: string,
    scope: string,
    kind: "content" | "room" | "sender" | "override" | "underride",
    ruleID: string,
    parameters: Pick<PushRule, "actions" | "conditions" | "pattern">,
    before?: string,
    after?: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<APIError>> {
    const url = generateQueriedURL(join(baseURL, endpoints.pushRule(scope, kind, ruleID)), { before, after });
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(url, response, requestInit, "There was a problem configuring this push rule.");
        case 429:
            try {
                return await handleRateLimitRetry(
                    response,
                    setPushRule,
                    [token, scope, kind, ruleID, parameters, before, after, 0, 0, baseURL],
                    retryDelay,
                    maxRetries
                );
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Push Rule Actions
 * The actions for a push rule
 *
 * ### Implementation Notes
 * This endpoint get the actions for the specified push rule.
 *
 * @param scope `global` to specify global rules.
 * @param kind The kind of rule.
 * @param ruleID The identifier for the rule.
 * @returns The actions for this push rule
 */
export async function getPushRuleActions(
    token: string,
    scope: string,
    kind: "content" | "room" | "sender" | "override" | "underride",
    ruleID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The action(s) to perform for this rule. */
          actions: string[];
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.pushRuleActions(scope, kind, ruleID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Push Rule Actions
 * Set the actions for a push rule.
 *
 * ### Implementation Notes
 * This endpoint allows clients to change the actions of a push rule.
 * This can be used to change the actions of builtin rules.
 *
 * @param scope `global` to specify global rules.
 * @param kind The kind of rule.
 * @param ruleID The identifier for the rule.
 * @returns The actions for the push rule were set.
 */
export async function setPushRuleActions(
    token: string,
    scope: string,
    kind: "content" | "room" | "sender" | "override" | "underride",
    ruleID: string,
    parameters: {
        /** The action(s) to perform for this rule. */
        actions: string[];
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.pushRuleActions(scope, kind, ruleID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Is Push Rule Enabled
 * Get whether a push rule is enabled.
 *
 * ### Implementation Notes
 * This endpoint gets whether the specified push rule is enabled.
 *
 * @param scope `global` to specify global rules.
 * @param kind The kind of rule.
 * @param ruleID The identifier for the rule.
 * @returns Whether the push rule is enabled.
 */
export async function isPushRuleEnabled(
    token: string,
    scope: string,
    kind: "content" | "room" | "sender" | "override" | "underride",
    ruleID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** Whether the push rule is enabled or not. */
          enabled: boolean;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.pushRuleEnabled(scope, kind, ruleID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Push Rule Enabled
 * Enabled or disable a push rule.
 *
 * ### Implementation Notes
 * This endpoint allows clients to enable or disable the specified push rule.
 *
 * @param scope `global` to specify global rules.
 * @param kind The kind of rule.
 * @param ruleID The identifier for the rule.
 * @returns The push rule was enabled or disabled.
 */
export async function setPushRuleEnabled(
    token: string,
    scope: string,
    kind: "content" | "room" | "sender" | "override" | "underride",
    ruleID: string,
    parameters: {
        /** Whether the push rule is enabled or not. */
        enabled: boolean;
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.pushRuleEnabled(scope, kind, ruleID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

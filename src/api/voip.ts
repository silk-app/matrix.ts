import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";
import { join } from "../deps.ts";
import { RateLimitError, RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { generateAPIResponse, generateRequestInit, handleRateLimitRetry } from "../utils.ts";

/** ## Get Turn Server
 * Obtain TURN server credentials.
 *
 * ### Implementation Notes
 * This API provides credentials for the client to use when initiating calls.
 *
 * @returns The TURN server credentials.
 */
export async function getTurnServer(
    token: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The password to use. */
          password: string;
          /** The time-to-live in seconds */
          ttl: number;
          /** A list of TURN URIs */
          uris: string[];
          /** The username to use. */
          username: string;
      }>
    | RequestError<RateLimitError>
> {
    const url = join(baseURL, endpoints.turnServer);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, getTurnServer, [token, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

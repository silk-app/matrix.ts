import { RequestError } from "../errors.ts";
import { APIResponse, Events } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit } from "../utils.ts";
import { defaultBaseURL, endpoints, errorMessages } from "../constants.ts";

/** ## Send To Device
 * Send an event to a given set of devices.
 *
 * ### Implementation Notes
 * This endpoint is used to send send-to-device events to a set of client devices.
 *
 * @param eventType The type of event to send.
 * @param txnId The transaction ID for this event. Clients should generate an ID unique across requests with the same access token; it will be used by the server to ensure idempotency of requests.
 * @returns The message was successfully sent.
 */
export async function sendToDevice(
    token: string,
    eventType: string,
    txnId: string,
    parameters: {
        /**
         * The messages to send.
         * A map from user ID, to a map from device ID to message body.
         * The device ID may also be *, meaning all known devices for the user.
         */
        messages: {
            [userID: string]: {
                [deviceID: string]: Events["content"];
            };
        };
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.sendToDevice(eventType, txnId));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

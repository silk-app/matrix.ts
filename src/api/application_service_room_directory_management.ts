import { RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit } from "../utils.ts";
import { defaultBaseURL, endpoints, errorMessages } from "../constants.ts";

/** ## Update App Service Room Directory Visibility
 * Updates a room's visibility in the application service's room directory.
 *
 * ### Implementation notes
 * Updates the visibility of a given room on the application service's room directory.
 *
 * This API is similar to the room directory visibility API used by clients to update the homeserver's more general room directory.
 *
 * This API requires the use of an application service access token (`as_token`) instead of a typical client's access_token.
 * This API cannot be invoked by users who are not identified as application services.
 *
 * @param networkID The protocol (network) ID to update the room list for. This would have been provided by the application service as being listed as a supported protocol.
 * @param roomID The room ID to add to the directory.
 * @returns The room's directory visibility has been updated.
 */
export async function updateAppServiceRoomDirectoryVisibility(
    token: string,
    networkID: string,
    roomID: string,
    parameters: {
        /** Whether the room should be visible (public) in the directory or not (private). */
        visibility: "public" | "private";
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.appServiceRoomDirectoryVisibility(networkID, roomID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

// deno-lint-ignore-file camelcase

import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit, generateQueriedURL, handleRateLimitRetry } from "../utils.ts";
import { APIResponse, Events, m, Message, RoomEvents, RoomStateEvents } from "../types/mod.ts";
import { APIError, RateLimitError, RequestError } from "../errors.ts";

/** ## Get Events
 * Listen on the event stream.
 *
 * ### Implementation Notes
 * This will listen for new events and return them to the caller.
 * This will block until an event is received, or until the `timeout` is reached.
 *
 * @deprecated This endpoint was deprecated in r0 of this specification.
 * Clients should instead call the |/sync| API with a `since` parameter.
 * See the `migration guide`.
 *
 * @param from The token to stream from. This token is either from a previous request to this API or from the initial sync API.
 * @param timeout The maximum time in milliseconds to wait for an event.
 * @returns The events received, which may be none.
 */
export async function getEvents(
    token: string,
    from?: string,
    timeout?: boolean,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** An array of events. */
          chunk?: Events[];
          /**
           * A token which correlates to the last value in `chunk`.
           * This token should be used in the next request to `/events`.
           */
          end?: string;
          /**
           * A token which correlates to the first value in `chunk`.
           * This is usually the same token supplied to `from=`.
           */
          start?: string;
      }>
    | RequestError<APIError>
> {
    const url = generateQueriedURL(join(baseURL, endpoints.events), { from, timeout });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(url, response, requestInit, "Bad pagination `from` parameter.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get One Event
 * Get a single event by event ID.
 *
 * ### Implementation Notes
 * Get a single event based on `event_id`.
 * You must have permission to retrieve this event e.g. by being a member in the room for this event.
 *
 * @deprecated This endpoint was deprecated in r0 of this specification.
 * Clients should instead call the |/rooms/{roomId}/event/{eventId}| API or the |/rooms/{roomId}/context/{eventId}| API.
 *
 * @param eventID The event ID to get.
 * @returns The full event.
 */
export async function getOneEvent(
    token: string,
    eventID: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<Pick<Events, "content" | "type">> | RequestError> {
    const url = join(baseURL, endpoints.event(eventID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "The event was not found or you do not have permission to read this event.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Initial Sync
 * Get the user's current state.
 *
 * ### Implementation Notes
 * This returns the full state for this user, with an optional limit on the number of messages per room to return.
 *
 * @deprecated This endpoint was deprecated in r0 of this specification.
 * Clients should instead call the |/sync| API with no `since` parameter.
 * See the `migration guide`.
 *
 * @param limit The maximum number of messages to return for each room.
 * @param archive Whether to include rooms that the user has left. If `false` then only rooms that the user has been invited to or has joined are included. If set to `true` then rooms that the user has left are included as well. By default this is `false`.
 * @returns The user's current state.
 */
export async function initialSync(
    token: string,
    limit?: number,
    archive?: boolean,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The global private data created by this user. */
          account_data?: Events[]; // TODO : might be different
          /** A token which correlates to the last value in `chunk`. This token should be used with the `/events` API to listen for new events. */
          end: string;
          /** A list of presence events. */
          presence: m.presence[];
          rooms: {
              /** The private data that this user has attached to this room. */
              account_data?: Events[]; // TODO : might be different
              /** The invite event if membership is invite */
              invite?: m.room.member;
              /** The user's membership state in this room. */
              membership: "invite" | "join" | "leave" | "ban";
              /** The pagination chunk for this room. */
              messages?: {
                  /**
                   * If the user is a member of the room this will be a list of the most recent messages for this room.
                   * If the user has left the room this will be the messages that preceeded them leaving.
                   * This array will consist of at most `limit` elements.
                   */
                  chunk: Message[];
                  /**
                   * A token which correlates to the last value in `chunk`.
                   * Used for pagination.
                   */
                  end: string;
                  /**
                   * A token which correlates to the first value in `chunk`.
                   * Used for pagination.
                   */
                  start: string;
              };
              /** The ID of this room. */
              room_id: string;
              /**
               *  If the user is a member of the room this will be the current state of the room as a list of events.
               * If the user has left the room this will be the state of the room when they left it.
               */
              state?: RoomStateEvents[];
              /** Whether this room is visible to the `/publicRooms` API or not. */
              visibility?: "private" | "public";
          }[];
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.initialSync), { limit, archive });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "There is no avatar URL for this user or this user does not exist.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Event Context
 * Get events and state around the specified event.
 *
 * ### Implementation Notes
 * This API returns a number of events that happened just before and after the specified event.
 * This allows clients to get the context surrounding an event.
 *
 * Note: This endpoint supports lazy-loading of room member events.
 * See `Lazy-loading room members`_ for more information.
 *
 * @param roomID The room to get events from.
 * @param eventID The event to get context around.
 * @param limit The maximum number of events to return. Default: 10.
 * @param filter A JSON `RoomEventFilter` to filter the returned events with. The filter is only applied to `events_before`, `events_after`, and `state`. It is not applied to the `event` itself. The filter may be applied before or/and after the `limit` parameter - whichever the homeserver prefers.
 *
 * See `Filtering`_ for more information.
 * @returns The events and state surrounding the requested event.
 */
export async function getEventContext(
    token: string,
    roomID: string,
    eventID: string,
    limit?: number,
    filter?: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A token that can be used to paginate forwards with. */
          end?: string;
          /** Details of the requested event. */
          event?: RoomEvents;
          /** A list of room events that happened just after the requested event, in chronological order. */
          events_after?: RoomEvents[];
          /** A list of room events that happened just before the requested event, in reverse-chronological order. */
          events_before?: RoomEvents[];
          /** A token that can be used to paginate backwards with. */
          start?: string;
          /** The state of the room at the last event returned. */
          state?: RoomStateEvents[];
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.eventContext(roomID, eventID)), { limit, filter });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get One Room Event
 * Get a single event by event ID.
 *
 * ### Implementation Notes
 * Get a single event based on `roomId/eventId`.
 * You must have permission to retrieve this event e.g. by being a member in the room for this event.
 *
 * @param roomID The ID of the room the event is in.
 * @param eventID The event ID to get.
 * @returns The full event
 */
export async function getOneRoomEvent(
    token: string,
    roomID: string,
    eventID: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<RoomEvents> | RequestError> {
    const url = join(baseURL, endpoints.roomEvent(roomID, eventID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "The event was not found or you do not have permission to read this event.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Room Initial Sync
 * Snapshot the current state of a room and its most recent messages.
 *
 * ### Implementation Notes
 * Get a copy of the current state and the most recent messages in a room.
 *
 * @deprecated This endpoint was deprecated in r0 of this specification.
 * There is no direct replacement; the relevant information is returned by the |/sync| API.
 * See the `migration guide`.
 *
 * @param roomID The room to get the data.
 * @returns The current state of the room
 */
export async function roomInitialSync(
    token: string,
    roomID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The private data that this user has attached to this room. */
          account_data?: Events[]; // TODO : might be different
          /** The user's membership state in this room. */
          membership: "invite" | "join" | "leave" | "ban";
          /** The pagination chunk for this room. */
          messages?: {
              /**
               * If the user is a member of the room this will be a list of the most recent messages for this room.
               * If the user has left the room this will be the messages that preceeded them leaving.
               * This array will consist of at most `limit` elements.
               */
              chunk: Message[];
              /**
               * A token which correlates to the last value in `chunk`.
               * Used for pagination.
               */
              end: string;
              /**
               * A token which correlates to the first value in `chunk`.
               * Used for pagination.
               */
              start: string;
          };
          /** The ID of this room. */
          room_id: string;
          /**
           *  If the user is a member of the room this will be the current state of the room as a list of events.
           * If the user has left the room this will be the state of the room when they left it.
           */
          state?: RoomStateEvents[];
          /** Whether this room is visible to the `/publicRooms` API or not. */
          visibility?: "private" | "public";
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.roomInitialSync(roomID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(url, response, requestInit, "You aren't a member of the room and weren't previously a member of the room.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Joined Members By Room
 * Gets the list of currently joined users and their profile data.
 *
 * ### Implementation Notes
 * This API returns a map of MXIDs to member info objects for members of the room.
 * The current user must be in the room for it to work, unless it is an Application Service in which case any of the AS's users must be in the room.
 * This API is primarily for Application Services and should be faster to respond than `/members` as it can be implemented more efficiently on the server.
 *
 * @param roomID The room to get the members of.
 * @returns A map of MXID to room member objects.
 */
export async function getJoinedMembersByRoom(
    token: string,
    roomID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A map from user ID to a RoomMember object. */
          joined?: {
              [userID: string]: {
                  /** 	The mxc avatar url of the user this object is representing. */
                  avatar_url?: string;
                  /** The display name of the user this object is representing. */
                  display_name?: string;
              };
          };
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.roomJoinedMembers(roomID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(url, response, requestInit, "You aren't a member of the room.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Members By Room
 * Get the m.room.member events for the room.
 *
 * ### Implementation Notes
 * Get the list of members for this room.
 *
 * @param roomID The room to get the member events for.
 * @param at The point in time (pagination token) to return members for in the room. This token can be obtained from a `prev_batch` token returned for each room by the sync API. Defaults to the current state of the room, as determined by the server.
 * @param membership The kind of membership to filter for. Defaults to no filtering if unspecified. When specified alongside `not_membership`, the two parameters create an 'or' condition: either the membership is the same as `membership` **or** is not the same as `not_membership`.
 * @param notMembership The kind of membership to exclude from the results. Defaults to no filtering if unspecified.
 * @returns A list of members of the room. If you are joined to the room then this will be the current members of the room. If you have left the room then this will be the members of the room when you left.
 */
export async function getMembersByRoom(
    token: string,
    roomID: string,
    at?: string,
    membership?: "join" | "invite" | "leave" | "ban",
    notMembership?: "join" | "invite" | "leave" | "ban",
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A map from user ID to a RoomMember object. */
          joined?: {
              [userID: string]: {
                  /** 	The mxc avatar url of the user this object is representing. */
                  avatar_url?: string;
                  /** The display name of the user this object is representing. */
                  display_name?: string;
              };
          };
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.roomJoinedMembers(roomID)), { at, membership, not_membership: notMembership });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(url, response, requestInit, "You aren't a member of the room and weren't previously a member of the room.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Room Events
 * Get a list of events for this room
 *
 * ### Implementation Notes
 * This API returns a list of message and state events for a room.
 * It uses pagination query parameters to paginate history in the room.
 *
 * Note: This endpoint supports lazy-loading of room member events.
 * See `Lazy-loading room members`_ for more information.
 *
 * @param roomID The room to get events from.
 * @param dir The direction to return events from.
 * @param from The token to start returning events from. This token can be obtained from a `prev_batch` token returned for each room by the sync API, or from a `start` or `end` token returned by a previous request to this endpoint.
 * @param to The token to stop returning events at. This token can be obtained from a `prev_batch` token returned for each room by the sync endpoint, or from a `start` or `end` token returned by a previous request to this endpoint.
 * @param limit The maximum number of events to return. Default: 10.
 * @param filter A JSON RoomEventFilter to filter returned events with.
 * @returns A list of messages with a new token to request more.
 */
export async function getRoomEvents(
    token: string,
    roomID: string,
    dir: "b" | "r", // TODO : confirm whether the second option is "r" or "b" (see doc string for `chunk`, `end` and `start` properties of response)
    from: string,
    to?: string,
    limit?: number,
    filter?: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * A list of room events.
           * The order depends on the `dir` parameter.
           * For `dir=b` events will be in reverse-chronological order, for `dir=f` in chronological order, so that events start at the from point.
           */
          chunk?: RoomEvents[];
          /**
           * The token the pagination ends at.
           * If `dir=b` this token should be used again to request even earlier events.
           */
          end?: string;
          /**
           * The token the pagination starts from.
           * If `dir=b` this will be the token supplied in from.
           */
          start?: string;
          /**
           * A list of state events relevant to showing the `chunk`.
           * For example, if `lazy_load_members` is enabled in the filter then this may contain the membership events for the senders of events in the `chunk`.
           *
           * Unless `include_redundant_members` is `true`, the server may remove membership events which would have already been sent to the client in prior calls to this endpoint, assuming the membership of those members has not changed.
           */
          state?: RoomStateEvents[];
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.roomEvents(roomID)), { dir, from, to, limit, filter });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(url, response, requestInit, "You aren't a member of the room.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Post Receipt
 * Send a receipt for the given event ID.
 *
 * ### Implementation Notes
 * This API updates the marker for the given receipt type to the event ID specified.
 *
 * @param roomID The room in which to send the event.
 * @param receiptType The type of receipt to send.
 * @param eventID The event ID to acknowledge up to.
 * @returns The receipt was sent.
 */
export async function postReceipt(
    token: string,
    roomID: string,
    receiptType: "m.read",
    eventID: string,
    parameters?: any,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.receipt(roomID, receiptType, eventID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(
                    response,
                    postReceipt,
                    [token, roomID, receiptType, eventID, parameters, 0, 0, baseURL],
                    retryDelay,
                    maxRetries
                );
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Redact Event
 * Strips all non-integrity-critical information out of an event.
 *
 * ### Implementation Notes
 * Strips all information out of an event which isn't critical to the integrity of the server-side representation of the room.
 *
 * This cannot be undone.
 *
 * Users may redact their own events, and any user with a power level greater than or equal to the `redact` power level of the room may redact events there.
 *
 * @param roomID The room from which to redact the event.
 * @param eventID The ID of the event to redact
 * @param txnID The transaction ID for this event. Clients should generate a unique ID; it will be used by the server to ensure idempotency of requests.
 * @returns An ID for the redaction event.
 */
export async function redactEvent(
    token: string,
    roomID: string,
    eventID: string,
    txnID: string,
    parameters?: {
        /** The reason for the event being redacted. */
        reason?: string;
    },
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A unique identifier for the event. */
          event_id?: string;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.redactEvent(roomID, eventID, txnID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Send Message
 * Send a message event to the given room.
 *
 * ### Implementation Notes
 * This endpoint is used to send a message event to a room.
 * Message events allow access to historical events and pagination, making them suited for "once-off" activity in a room.
 *
 * The body of the request should be the content object of the event;
 * the fields in this object will vary depending on the type of event.
 * See `Room Events`_ for the m. event specification.
 *
 * @param roomID The room to send the event to.
 * @param eventType The type of event to send.
 * @param txnID The transaction ID for this event. Clients should generate an ID unique across requests with the same access token; it will be used by the server to ensure idempotency of requests.
 * @returns An ID for the sent event.
 */
export async function sendMessage(
    token: string,
    roomID: string,
    eventType: string,
    txnID: string,
    parameters?: Message["content"],
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A unique identifier for the event. */
          event_id: string;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.sendMessage(roomID, eventType, txnID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Room State
 * Get all state events in the current state of a room.
 *
 * ### Implementation Notes
 * Get the state events for the current state of a room.
 *
 * @param roomID The room to look up the state for.
 * @returns The current state of the room
 */
export async function getRoomState(token: string, roomID: string, baseURL: string = defaultBaseURL): Promise<APIResponse<RoomStateEvents[]> | RequestError> {
    const url = join(baseURL, endpoints.roomState(roomID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(url, response, requestInit, "You aren't a member of the room and weren't previously a member of the room.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Room State With Key
 * Get the state identified by the type and key.
 *
 * ### Implementation Notes
 * .. For backwards compatibility with older links... .. _`get-matrix-client-unstable-rooms-roomid-state-eventtype`:
 *
 * Looks up the contents of a state event in a room.
 * If the user is joined to the room then the state is taken from the current state of the room.
 * If the user has left the room then the state is taken from the state of the room when they left.
 *
 * @param roomID The room to look up the state in.
 * @param eventType The type of state to look up.
 * @param stateKey The key of the state to look up. Defaults to an empty string. When an empty string, the trailing slash on this endpoint is optional.
 * @returns The content of the state event.
 */
export async function getRoomStateWithKey(
    token: string,
    roomID: string,
    eventType: string,
    stateKey: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<RoomStateEvents["content"]> | RequestError> {
    const url = join(baseURL, endpoints.roomStateWithKey(roomID, eventType, stateKey));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(url, response, requestInit, "You aren't a member of the room and weren't previously a member of the room.");
        case 404:
            return new RequestError(url, response, requestInit, "The room has no state with the given type or key.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Room State With Key
 * Send a state event to the given room.
 *
 * ### Implementation Notes
 * .. For backwards compatibility with older links... .. _`put-matrix-client-unstable-rooms-roomid-state-eventtype`:
 *
 * State events can be sent using this endpoint.
 * These events will be overwritten if , and ```` all match.
 *
 * Requests to this endpoint **cannot use transaction IDs** like other PUT paths because they cannot be differentiated from the `state_key`.
 * Furthermore, `POST` is unsupported on state paths.
 *
 * The body of the request should be the content object of the event; the fields in this object will vary depending on the type of event.
 * See `Room Events`_ for the `m.` event specification.
 *d
 * If the event type being sent is `m.room.canonical_alias` servers SHOULD ensure that any new aliases being listed in the event are valid per their grammar/syntax and that they point to the room ID where the state event is to be sent.
 * Servers do not validate aliases which are being removed or are already present in the state event.
 *
 * @param roomID The room to set the state in
 * @param eventType The type of event to send.
 * @param stateKey The state_key for the state to send. Defaults to the empty string. When an empty string, the trailing slash on this endpoint is optional.
 * @returns An ID for the sent event.
 */
export async function setRoomStateWithKey(
    token: string,
    roomID: string,
    eventType: string,
    stateKey: string,
    parameters?: RoomStateEvents["content"],
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A unique identifier for the event. */
          event_id: string;
      }>
    | RequestError<APIError>
> {
    const url = join(baseURL, endpoints.roomStateWithKey(roomID, eventType, stateKey));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(
                url,
                response,
                requestInit,
                `The sender's request is malformed.

Some example error codes include:
- \`M_INVALID_PARAMETER\`: One or more aliases within the \`m.room.canonical_alias\` event have invalid syntax.
- \`M_BAD_ALIAS\`: One or more aliases within the \`m.room.canonical_alias\` event do not point to the room ID for which the state event is to be sent to.`,
                await response.json()
            );
        case 403:
            return new RequestError(url, response, requestInit, "The sender doesn't have permission to send the event into the room.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Typing
 * Informs the server that the user has started or stopped typing.
 *
 * ### Implementation Notes
 * This tells the server that the user is typing for the next N milliseconds where N is the value specified in the `timeout` key.
 * Alternatively, if `typing` is `false`, it tells the server that the user has stopped typing.
 *
 * @param userID The user who has started to type.
 * @param roomID The room in which the user is typing.
 * @returns The new typing state was set.
 */
export async function setTyping(
    token: string,
    roomID: string,
    userID: string,
    parameters: {
        /** The length of time in milliseconds to mark this user as typing. */
        timeout?: number;
        /**
         * Whether the user is typing or not.
         * If false, the timeout key can be omitted.
         */
        typing: boolean;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.typing(roomID, userID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, setTyping, [token, roomID, userID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                throw error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Sync
 * Synchronise the client's state and receive new messages.
 *
 * ### Implementation Notes
 * Synchronise the client's state with the latest state on the server.
 * Clients use this API when they first log in to get an initial snapshot of the state on the server, and then continue to call this API to get incremental deltas to the state, and to receive new messages.
 *
 * Note: This endpoint supports lazy-loading.
 * See `Filtering`_ for more information.
 * Lazy-loading members is only supported on a `StateFilter` for this endpoint.
 * When lazy-loading is enabled, servers MUST include the syncing user's own membership event when they join a room, or when the full state of rooms is requested, to aid discovering the user's avatar & displayname.
 *
 * Like other members, the user's own membership event is eligible for being considered redundant by the server.
 * When a sync is `limited`, the server MUST return membership events for events in the gap (between since and the start of the returned timeline), regardless as to whether or not they are redundant.
 * This ensures that joins/leaves and profile changes which occur during the gap are not lost.
 *
 * @param filter The ID of a filter created using the filter API or a filter JSON object encoded as a string. The server will detect whether it is an ID or a JSON object by whether the first character is a "{" open brace. Passing the JSON inline is best suited to one off requests. Creating a filter using the filter API is recommended for clients that reuse the same filter multiple times, for example in long poll requests.
 *
 * See `Filtering`_ for more information.
 *
 * @param since A point in time to continue a sync from.
 * @param full_state Controls whether to include the full state for all rooms the user is a member of.
 *
 * If this is set to `true`, then all state events will be returned, even if `since` is non-empty. The timeline will still be limited by the `since` parameter. In this case, the `timeout` parameter will be ignored and the query will return immediately, possibly with an empty timeline.
 *
 * If `false`, and `since` is non-empty, only state which has changed since the point indicated by `since` will be returned.
 *
 * By default, this is `false`.
 * @param set_presence Controls whether the client is automatically marked as online by polling this API. If this parameter is omitted then the client is automatically marked as online when it uses this API. Otherwise if the parameter is set to "offline" then the client is not marked as being online when it uses this API. When set to "unavailable", the client is marked as being idle.
 * @param timeout The maximum time to wait, in milliseconds, before returning this request. If no events (or other data) become available before this time elapses, the server will return a response with empty fields.
 *
 * By default, this is `0`, so the server will return immediately even if the response is empty.
 * @returns The initial snapshot or delta for the client to use to update their state.
 */
export async function sync(
    token: string,
    filter?: string,
    since?: string,
    fullState?: boolean,
    setPresence?: "offline" | "online" | "unavailable",
    timeout?: number,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The global private data created by this user. */
          account_data?: {
              /** List of events. */
              events?: Events[];
          };
          /** Information on end-to-end device updates, as specified in |devicelists_sync|. */
          device_lists?: {
              /** List of users who have updated their device identity keys, or who now share an encrypted room with the client since the previous sync response. */
              changed?: string[];
              /** List of users with whom we do not share any encrypted rooms anymore since the previous sync response. */
              left?: string[];
          };
          /** Information on end-to-end encryption keys, as specified in |devicelists_sync|. */
          device_one_time_keys_count?: {
              [key: string]: number;
          };
          /** The batch token to supply in the `since` param of the next `/sync` request. */
          next_batch: string;
          /** The updates to the presence status of other users. */
          presence?: {
              /** List of events. */
              events?: Events[];
          };
          /** Updates to rooms. */
          rooms?: {
              /** The rooms that the user has been invited to, mapped as room ID to room information. */
              invite?: {
                  [roomID: string]: {
                      /**
                       * The state of a room that the user has been invited to.
                       * These state events may only have the `sender`, `type`, `state_key` and `content` keys present.
                       * These events do not replace any state that the client already has for the room, for example if the client has archived the room.
                       * Instead the client should keep two separate copies of the state: the one from the `invite_state` and one from the archived `state`.
                       * If the client joins the room then the current state will be given as a delta against the archived `state` not the `invite_state`.
                       */
                      invite_state?: {
                          /** The StrippedState events that form the invite state. */
                          events?: Pick<RoomStateEvents, "content" | "state_key" | "type" | "sender">[];
                      };
                  };
              };
              /** The rooms that the user has joined, mapped as room ID to room information. */
              join?: {
                  [roomID: string]: {
                      /** Information about the room which clients may need to correctly render it to users. */
                      summary?: {
                          /**
                           * The users which can be used to generate a room name if the room does not have one.
                           * Required if the room's `m.room.name` or `m.room.canonical_alias` state events are unset or empty.
                           *
                           * This should be the first 5 members of the room, ordered by stream ordering, which are joined or invited.
                           * The list must never include the client's own user ID.
                           * When no joined or invited members are available, this should consist of the banned and left users.
                           * More than 5 members may be provided, however less than 5 should only be provided when there are less than 5 members to represent.
                           *
                           * When lazy-loading room members is enabled, the membership events for the heroes MUST be included in the state, unless they are redundant.
                           * When the list of users changes, the server notifies the client by sending a fresh list of heroes.
                           * If there are no changes since the last sync, this field may be omitted.
                           */
                          "m.heroes"?: string[];
                          /**
                           * The number of users with `membership` of `join`, including the client's own user ID.
                           * If this field has not changed since the last sync, it may be omitted.
                           * Required otherwise.
                           */
                          "m.joined_member_count"?: number;
                          /**
                           * The number of users with `membership` of `invite`.
                           * If this field has not changed since the last sync, it may be omitted.
                           * Required otherwise.
                           */
                          "m.invited_member_count"?: number;
                      };
                      /**
                       * Updates to the state, between the time indicated by the `since` parameter, and the start of the `timeline` (or all state up to the start of the `timeline`, if `since` is not given, or `full_state` is true).
                       *
                       * N.B. state updates for `m.room.member` events will be incomplete if `lazy_load_members` is enabled in the /sync filter, and only return the member events required to display the senders of the timeline events in this response.
                       */
                      state?: {
                          /** List of events. */
                          events?: RoomStateEvents[];
                      };
                      /** The timeline of messages and state changes in the room. */
                      timeline?: {
                          /** List of events. */
                          events?: RoomEvents[];
                          /** True if the number of events returned was limited by the `limit` on the filter. */
                          limited?: boolean;
                          /** A token that can be supplied to the from parameter of the rooms/{roomId}/messages endpoint. */
                          prev_batch?: string;
                      };
                      /** The ephemeral events in the room that aren't recorded in the timeline or state of the room. e.g. typing. */
                      ephemeral?: {
                          /** List of events. */
                          events?: RoomEvents[];
                      };
                      /** The private data that this user has attached to this room. */
                      account_data?: {
                          events?: Events[]; // TODO : May be different
                      };
                      /**
                       * Counts of unread notifications for this room.
                       * See the `Receiving notifications section` for more information on how these are calculated.
                       */
                      unread_notifications?: {
                          /** The number of unread notifications for this room with the highlight flag set */
                          highlight_count?: number;
                          /** The total number of unread notifications for this room */
                          notification_count?: number;
                      };
                  };
              };
              /** The rooms that the user has left or been banned from, mapped as room ID to room information. */
              leave?: {
                  [roomID: string]: {
                      /** The state updates for the room up to the start of the timeline. */
                      state?: {
                          /** List of events. */
                          events?: RoomStateEvents[];
                      };
                      /** The timeline of messages and state changes in the room up to the point when the user left. */
                      timeline?: {
                          /** List of events. */
                          events?: RoomEvents[];
                          /** True if the number of events returned was limited by the `limit` on the filter. */
                          limited?: boolean;
                          /** A token that can be supplied to the from parameter of the rooms/{roomId}/messages endpoint. */
                          prev_batch?: string;
                      };
                      /** The private data that this user has attached to this room. */
                      account_data?: {
                          events?: Events[]; // TODO : May be different
                      };
                  };
              };
          };
          /** Information on the send-to-device messages for the client device, as defined in |sendto_device_sync|. */
          to_device?: {
              /** List of send-to-device messages. */
              events?: Events[];
          };
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.sync), { filter, since, full_state: fullState, set_presence: setPresence, timeout });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

export type EventFilter = {
    /** The maximum number of events to return. */
    limit?: number;
    /**
     * A list of sender IDs to exclude.
     * If this list is absent then no senders are excluded.
     * A matching sender will be excluded even if it is listed in the `'senders'` filter.
     */
    not_senders?: string[];
    /**
     * A list of event types to exclude.
     * If this list is absent then no event types are excluded.
     * A matching type will be excluded even if it is listed in the `'types'` filter.
     * A '*' can be used as a wildcard to match any sequence of characters.
     */
    not_types?: string[];
    /**
     * A list of senders IDs to include.
     * If this list is absent then all senders are included.
     */
    senders?: string[];
    /**
     * A list of event types to include.
     * If this list is absent then all event types are included.
     * A `'*'` can be used as a wildcard to match any sequence of characters.
     */
    types?: string[];
};

export type StateFilter = {
    /** The maximum number of events to return. */
    limit?: number;
    /**
     * A list of sender IDs to exclude.
     * If this list is absent then no senders are excluded.
     * A matching sender will be excluded even if it is listed in the `'senders'` filter.
     */
    not_senders?: string[];
    /**
     * A list of event types to exclude.
     * If this list is absent then no event types are excluded.
     * A matching type will be excluded even if it is listed in the `'types'` filter.
     * A '*' can be used as a wildcard to match any sequence of characters.
     */
    not_types?: string[];
    /**
     * A list of senders IDs to include.
     * If this list is absent then all senders are included.
     */
    senders?: string[];
    /**
     * A list of event types to include.
     * If this list is absent then all event types are included.
     * A `'*'` can be used as a wildcard to match any sequence of characters.
     */
    types?: string[];
    /**
     * If `true`, includes only events with a `url` key in their content.
     * If `false`, excludes those events
     * If omitted, `url` key is not considered for filtering.
     */
    contains_url?: boolean;
    /**
     * If `true`, sends all membership events for all events, even if they have already been sent to the client.
     * Does not apply unless `lazy_load_members` is `true`.
     * See `Lazy-loading room members`_ for more information.
     * Defaults to `false`.
     */
    include_redundant_members?: boolean;
    /**
     * If `true`, enables lazy-loading of membership events.
     * See `Lazy-loading room members`_ for more information.
     * Defaults to `false`.
     */
    lazy_load_members?: boolean;
    /**
     * A list of room IDs to exclude.
     * If this list is absent then no rooms are excluded.
     * A matching room will be excluded even if it is listed in the `'rooms'` filter.
     */
    not_rooms?: string[];
    /**
     * A list of room IDs to include.
     * If this list is absent then all rooms are included.
     */
    rooms?: string[];
};

export type RoomFilter = {
    /** The per user account data to include for rooms. */
    account_data?: EventFilter;
    /** The events that aren't recorded in the room history, e.g. typing and receipts, to include for rooms. */
    ephemeral?: EventFilter;
    /** Include rooms that the user has left in the sync, default false */
    include_leave?: boolean;
    /**
     * A list of room IDs to exclude.
     * If this list is absent then no rooms are excluded.
     * A matching room will be excluded even if it is listed in the `'rooms'` filter.
     * This filter is applied before the filters in `ephemeral`, `state`, `timeline` or `account_data`
     */
    not_rooms?: string[];
    /**
     * A list of room IDs to include.
     * If this list is absent then all rooms are included.
     * This filter is applied before the filters in `ephemeral`, `state`, `timeline` or `account_data`
     */
    rooms?: string[];
    /** The state events to include for rooms. */
    state?: StateFilter;
    /** The message and state update events to include for rooms. */
    timeline?: EventFilter;
};

export type Filter = {
    /** The user account data that isn't associated with rooms to include. */
    account_data?: EventFilter;
    /**
     * List of event fields to include.
     * If this list is absent then all fields are included.
     * The entries may include '.' characters to indicate sub-fields.
     * So ['content.body'] will include the 'body' field of the 'content' object.
     * A literal '.' character in a field name may be escaped using a '\'.
     * A server may include more fields than were requested.
     */
    event_fields?: string[];
    /**
     * The format to use for events.
     * 'client' will return the events in a format suitable for clients.
     * 'federation' will return the raw event as received over federation.
     * The default is 'client'.
     */
    event_format?: "client" | "federation";
    /** The presence updates to include. */
    presence?: EventFilter;
    /** Filters to be applied to room data. */
    room?: RoomFilter;
};

/** ## Define Filter
 * Upload a new filter.
 *
 * ### Implementation Notes
 * Uploads a new filter definition to the homeserver.
 * Returns a filter ID that may be used in future requests to restrict which events are returned to the client.
 *
 * @param userID The id of the user uploading the filter. The access token must be authorized to make requests for this user id.
 * @returns The filter was created.
 */
export async function defineFilter(
    token: string,
    userID: string,
    parameters: Filter,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * The ID of the filter that was created.
           * Cannot start with a `{` as this character is used to determine if the filter provided is inline JSON or a previously declared filter by homeservers on some APIs.
           */
          filter_id: string;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.defineFilter(userID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Filter
 * Download a filter
 *
 * @param userID The user ID to download a filter for.
 * @param filterID The filter ID to download.
 * @returns "The filter definition"
 */
export async function getFilter(
    token: string,
    userID: string,
    filterID: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<Filter> | RequestError> {
    const url = join(baseURL, endpoints.filter(userID, filterID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "Unknown filter.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

import { RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateQueriedURL, generateRequestInit } from "../utils.ts";
import { defaultBaseURL, endpoints, errorMessages } from "../constants.ts";

/** ## Get Public Rooms
 * Lists the public rooms on the server.
 *
 * ### Implementation Notes
 * Lists the public rooms on the server.
 *
 * This API returns paginated responses.
 * The rooms are ordered by the number of joined members, with the largest rooms first.
 *
 * @param limit Limit the number of results returned.
 * @param since A pagination token from a previous request, allowing clients to get the next (or previous) batch of rooms. The direction of pagination is specified solely by which token is supplied, rather than via an explicit flag.
 * @param server The server to fetch the public room lists from. Defaults to the local server.
 * @returns A list of the rooms on the server.
 */
export async function getPublicRooms(
    limit?: number,
    since?: string,
    server?: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A paginated chunk of public rooms. */
          chunk: {
              /**
               * Aliases of the room.
               * May be empty.
               */
              aliases?: string[];
              /** The URL for the room's avatar, if one is set. */
              avatar_url?: string;
              /** The canonical alias of the room, if any. */
              canonical_alias?: string;
              /**
               * Whether guest users may join the room and participate in it.
               * If they can, they will be subject to ordinary power level rules like any other user.
               */
              guest_can_join: boolean;
              /** The name of the room, if any. */
              name?: string;
              /** The number of members joined to the room. */
              num_joined_members: number;
              /** The ID of the room. */
              room_id: string;
              /** The topic of the room, if any. */
              topic?: string;
              /** Whether the room may be viewed by guest users without joining. */
              world_readable: boolean;
          }[];
          /**
           * A pagination token for the response.
           * The absence of this token means there are no more results to fetch and the client should stop paginating.
           */
          next_batch?: string;
          /**
           * A pagination token that allows fetching previous results.
           * The absence of this token means there are no results before this batch, i.e. this is the first batch.
           */
          prev_batch?: string;
          /** An estimate on the total number of public rooms, if the server has an estimate. */
          total_room_count_estimate?: number;
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.publicRooms), { limit, since, server });
    const requestInit = generateRequestInit("GET", undefined);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Query Public Rooms
 * Lists the public rooms on the server with optional filter.
 *
 * ### Implementation Notes
 * Lists the public rooms on the server, with optional filter.
 *
 * This API returns paginated responses.
 * The rooms are ordered by the number of joined members, with the largest rooms first.
 *
 * @param server The server to fetch the public room lists from. Defaults to the local server.
 * @returns A list of the rooms on the server.
 */
export async function queryPublicRooms(
    token: string,
    parameters: {
        /** Filter to apply to the results */
        filter?: {
            /** A string to search for in the room metadata, e.g. name, topic, canonical alias etc. */
            generic_search_term?: string;
        };
        /**
         * Whether or not to include all known networks/protocols from application services on the homeserver.
         * Defaults to false.
         */
        include_all_networks?: boolean;
        /** Limit the number of results returned. */
        limit?: number;
        /**
         * A pagination token from a previous request, allowing clients to get the next (or previous) batch of rooms.
         * The direction of pagination is specified solely by which token is supplied, rather than via an explicit flag.
         */
        since?: string;
        /**
         * The specific third party network/protocol to request from the homeserver.
         * Can only be used if `include_all_networks` is false.
         */
        third_party_instance_id?: string;
    },
    server?: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A paginated chunk of public rooms. */
          chunk: {
              /**
               * Aliases of the room.
               * May be empty.
               */
              aliases?: string[];
              /** The URL for the room's avatar, if one is set. */
              avatar_url?: string;
              /** The canonical alias of the room, if any. */
              canonical_alias?: string;
              /**
               * Whether guest users may join the room and participate in it.
               * If they can, they will be subject to ordinary power level rules like any other user.
               */
              guest_can_join: boolean;
              /** The name of the room, if any. */
              name?: string;
              /** The number of members joined to the room. */
              num_joined_members: number;
              /** The ID of the room. */
              room_id: string;
              /** The topic of the room, if any. */
              topic?: string;
              /** Whether the room may be viewed by guest users without joining. */
              world_readable: boolean;
          }[];
          /**
           * A pagination token for the response.
           * The absence of this token means there are no more results to fetch and the client should stop paginating.
           */
          next_batch?: string;
          /**
           * A pagination token that allows fetching previous results.
           * The absence of this token means there are no results before this batch, i.e. this is the first batch.
           */
          prev_batch?: string;
          /** An estimate on the total number of public rooms, if the server has an estimate. */
          total_room_count_estimate?: number;
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.publicRooms), { server });
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

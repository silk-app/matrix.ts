import { RequestError } from "../errors.ts";
import { APIResponse, m } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit } from "../utils.ts";
import { defaultBaseURL, endpoints, errorMessages } from "../constants.ts";

/** ## Get Well Known
 * Gets Matrix server discovery information about the domain.
 *
 * ### Implementation Notes
 * Gets discovery information about the domain.
 * The file may include additional keys, which MUST follow the Java package naming convention, e.g. `com.example.myapp.property`.
 * This ensures property names are suitably namespaced for each application and reduces the risk of clashes.
 *
 * Note that this endpoint is not necessarily handled by the homeserver, but by another webserver, to be used for discovering the homeserver URL.
 *
 * @returns Server discovery information.
 */
export async function getWellknown(
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          [property: string]: any;
          /** Used by clients to discover homeserver information. */
          "m.homeserver": {
              /** The base URL for the homeserver for client-server connections. */
              base_url: string;
          };
          /** Used by clients to discover identity server information. */
          "m.identity_server"?: m.identity_server["content"];
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.wellKnown);
    const response = await fetch(url);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, undefined, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Who Is
 * Gets information about a particular user.
 *
 * ### Implementation Notes
 * Gets information about a particular user.
 *
 * This API may be restricted to only be called by the user being looked up, or by a server admin. Server-local administrator privileges are not specified in this document.
 *
 * @param userID The user to look up.
 * @returns The lookup was successful.
 */
export async function getWhoIs(
    token: string,
    userID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The Matrix user ID of the user. */
          user_id?: string;
          /** Each key is an identifier for one of the user's devices. */
          devices?: {
              [key: string]: {
                  /** A user's sessions (i.e. what they did with an access token from one login). */
                  sessions?: [
                      {
                          /** Information particular connections in the session. */
                          connections?: [
                              {
                                  /** Most recently seen IP address of the session. */
                                  ip?: string;
                                  /** Unix timestamp that the session was last active. */
                                  last_seen?: number;
                                  /** User agent string last seen in the session. */
                                  user_agent?: string;
                              }
                          ];
                      }
                  ];
              };
          };
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.whoIs(userID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Versions
 * Gets the versions of the specification supported by the server.
 *
 * ### Implementation Notes
 * Gets the versions of the specification supported by the server.
 *
 * Values will take the form `rX.Y.Z`.
 *
 * Only the latest `Z` value will be reported for each supported `X.Y` value.
 * i.e. if the server implements `r0.0.0`, `r0.0.1`, and `r1.2.0`, it will report `r0.0.1` and `r1.2.0`.
 *
 * The server may additionally advertise experimental features it supports through `unstable_features`.
 * These features should be namespaced and may optionally include version information within their name if desired.
 * Features listed here are not for optionally toggling parts of the Matrix specification and should only be used to advertise support for a feature which has not yet landed in the spec.
 * For example, a feature currently undergoing the proposal process may appear here and eventually be taken off this list once the feature lands in the spec and the server deems it reasonable to do so.
 * Servers may wish to keep advertising features here after they've been released into the spec to give clients a chance to upgrade appropriately.
 * Additionally, clients should avoid using unstable features in their stable releases.
 *
 * @returns The versions supported by the server.
 */
export async function getVersions(
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** Experimental features the server supports. Features not listed here, or the lack of this property all together, indicate that a feature is not supported. */
          unstable_features?: {
              [key: string]: boolean;
          };
          /** The supported versions. */
          versions: string[];
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.versions);
    const response = await fetch(url);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, undefined, errorMessages.statusCodeNotImplemented);
    }
}

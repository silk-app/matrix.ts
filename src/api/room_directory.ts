import { APIError, RateLimitError, RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit, handleRateLimitRetry } from "../utils.ts";
import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";

/** ## Delete Room Alias
 * Remove a mapping of room alias to room ID.
 *
 * ### Implementation Notes
 * Remove a mapping of room alias to room ID.
 *
 * Servers may choose to implement additional access control checks here, for instance that room aliases can only be deleted by their creator or a server administrator.
 *
 * .. Note:: Servers may choose to update the `alt_aliases` for the `m.room.canonical_alias` state event in the room when an alias is removed.
 * Servers which choose to update the canonical alias event are recommended to, in addition to their other relevant permission checks,
 * delete the alias and return a successful response even if the user does not have permission to update the `m.room.canonical_alias` event.
 *
 * @param roomAlias The room alias to remove.
 * @returns The mapping was deleted.
 */
export async function deleteRoomAlias(token: string, roomAlias: string, baseURL: string = defaultBaseURL): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.roomAlias(roomAlias));
    const requestInit = generateRequestInit("DELETE", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "There is no mapped room ID for this room alias.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Room ID By Alias
 * Get the room ID corresponding to this room alias.
 *
 * ### Implementation Notes
 * Requests that the server resolve a room alias to a room ID.
 *
 * The server will use the federation API to resolve the alias if the domain part of the alias does not correspond to the server's own domain.
 *
 * @param roomAlias The room alias.
 * @returns The room ID and other information for this alias.
 */
export async function getRoomIDByAlias(
    token: string,
    roomAlias: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The room ID for this room alias. */
          room_id?: string;
          /** A list of servers that are aware of this room alias. */
          servers?: string[];
      }>
    | RequestError<APIError>
> {
    const url = join(baseURL, endpoints.roomAlias(roomAlias));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "There is no mapped room ID for this room alias.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Room Alias
 * Create a new mapping from room alias to room ID.
 *
 * @param roomAlias The room alias.
 *
 * @returns The mapping was created.
 */
export async function setRoomAlias(
    token: string,
    roomAlias: string,
    parameters: {
        /** The room ID to set. */
        room_id: string;
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<APIError>> {
    const url = join(baseURL, endpoints.roomAlias(roomAlias));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 409:
            return new RequestError(url, response, requestInit, "A room alias with that name already exists.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Local Aliases
 * Get a list of local aliases on a given room.
 *
 * ### Implementation Notes
 *  Get a list of aliases maintained by the local server for the given room.
 *
 * This endpoint can be called by users who are in the room (external users receive an `M_FORBIDDEN` error response).
 * If the room's `m.room.history_visibility maps` to `world_readable`, any user can call this endpoint.
 *
 * Servers may choose to implement additional access control checks here, such as allowing server administrators to view aliases regardless of membership.
 *
 * .. Note:: Clients are recommended not to display this list of aliases prominently as they are not curated, unlike those listed in the `m.room.canonical_alias` state event.
 *
 * @param roomID The room ID to find local aliases of.
 * @returns The list of local aliases for the room.
 */
export async function getLocalAliases(
    token: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    roomID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The server's local aliases on the room. Can be empty. */
          aliases: string[];
      }>
    | RequestError<APIError | RateLimitError>
> {
    const url = join(baseURL, endpoints.localAliases(roomID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                "The user is not permitted to retrieve the list of local aliases for the room.",
                await response.json()
            );
        case 429:
            try {
                return await handleRateLimitRetry(response, getLocalAliases, [token, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

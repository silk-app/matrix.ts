import { RateLimitError, RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit, handleRateLimitRetry } from "../utils.ts";
import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";

/** ## Request OpenID Token
 * Get an OpenID token object to verify the requester's identity.
 *
 * ### Implementation Notes
 * Gets an OpenID token object that the requester may supply to another service to verify their identity in Matrix.
 * The generated token is only valid for exchanging for user information from the federation API for OpenID.
 * The access token generated is only valid for the OpenID API.
 * It cannot be used to request another OpenID access token or call `/sync`, for example.
 *
 * @param userID The user to request and OpenID token for. Should be the user who is authenticated for the request.
 * @returns OpenID token information. This response is nearly compatible with the response documented in the `OpenID 1.0 Specification`_ with the only difference being the lack of an `id_token`. Instead, the Matrix homeserver's name is provided.
 */
export async function requestOpenIdToken(
    token: string,
    userID: string,
    // deno-lint-ignore ban-types (Empty object reserved for future expansion)
    parameters: {},
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * An access token the consumer may use to verify the identity of the person who generated the token.
           * This is given to the federation API `GET /openid/userinfo` to verify the user's identity.
           */
          access_token: string;
          /** The number of seconds before this token expires and a new one must be generated. */
          expires_in: number;
          /**  The homeserver domain the consumer should use when attempting to verify the user's identity. */
          matrix_server_name: string;
          /** The string `Bearer`. */
          token_type: string;
      }>
    | RequestError<RateLimitError>
> {
    const url = join(baseURL, endpoints.requestOpenIdToken(userID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, requestOpenIdToken, [token, userID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

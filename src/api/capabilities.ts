import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";
import { join } from "../deps.ts";
import { RateLimitError, RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { generateAPIResponse, generateRequestInit, handleRateLimitRetry } from "../utils.ts";

/** ## Get Capabilities
 * Gets information about the server's capabilities.
 *
 * ### Implementation Notes
 * Gets information about the server's supported feature set and other relevant capabilities.
 *
 * @returns The capabilities of the server.
 */
export async function getCapabilities(
    token: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The custom capabilities the server supports, using the Java package naming convention. */
          capabilities: {
              /** Capability to indicate if the user can change their password. */
              "m.change_password"?: {
                  /** True if the user can change their password, false otherwise. */
                  enabled: boolean;
              };
              /** The room versions the server supports. */
              "m.room_versions"?: {
                  /** A detailed description of the room versions the server supports. */
                  available: {
                      [version: string]: "stable" | "unstable";
                  };
                  /** The default room version the server is using for new rooms. */
                  default: string;
              };
          };
      }>
    | RequestError<RateLimitError>
> {
    const url = join(baseURL, endpoints.capabilities);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, getCapabilities, [token, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

import { APIError, RateLimitError, RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit, handleRateLimitRetry } from "../utils.ts";
import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";

/** ## Get Presence
 * Get this user's presence state.
 *
 * ### Implementation Notes
 * Get the given user's presence state.
 *
 * @param userID The user whose presence state to get.
 * @returns The presence state for this user.
 */
export async function getPresence(
    token: string,
    userID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** Whether the user is currently active */
          currently_active?: boolean;
          /** The length of time in milliseconds since an action was performed by this user. */
          last_active_ago?: number;
          /** This user's presence. */
          presence: "online" | "offline" | "unavailable";
          /** The state message for this user if one was set. */
          status_msg?: string;
      }>
    | RequestError<APIError>
> {
    const url = join(baseURL, endpoints.presence(userID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                join(baseURL, endpoints.presence(userID)),
                response,
                requestInit,
                "You are not allowed to see this user's presence status.",
                await response.json()
            );
        case 404:
            return new RequestError(
                join(baseURL, endpoints.presence(userID)),
                response,
                requestInit,
                "There is no presence state for this user. This user may not exist or isn't exposing presence information to you.",
                await response.json()
            );
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Presence
 * Update this user's presence state.
 *
 * ### Implementation Notes
 * This API sets the given user's presence state.
 * When setting the status, the activity time is updated to reflect that activity; the client does not need to specify the `last_active_ago` field.
 * You cannot set the presence state of another user.
 *
 * @param userID The user whose presence state to update.
 * @returns The new presence was set.
 */
export async function setPresence(
    token: string,
    userID: string,
    parameters: {
        /** The new presence state. */
        presence: "online" | "offline" | "unavailable";
        /** The status message to attach to this state. */
        status_msg?: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.presence(userID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, setPresence, [token, userID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

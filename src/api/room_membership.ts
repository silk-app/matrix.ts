import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";
import { join } from "../deps.ts";
import { APIError, RateLimitError, RequestError } from "../errors.ts";
import { APIResponse, Signatures } from "../types/mod.ts";
import { generateAPIResponse, generateQueriedURL, generateRequestInit, handleRateLimitRetry } from "../utils.ts";

/** ## Join Room
 * Start the requesting user participating in a particular room.
 *
 * ### Implementation Notes
 * Note that this API takes either a room ID or alias, unlike `/room/{roomId}/join`.
 *
 * This API starts a user participating in a particular room, if that user is allowed to participate in that room. After this call, the client is allowed to see all current state events in the room, and all subsequent events associated with the room until the user leaves the room.
 *
 * After a user has joined a room, the room will appear as an entry in the response of the |/initialSync| and |/sync| APIs.
 *
 * If a `third_party_signed` was supplied, the homeserver must verify that it matches a pending `m.room.third_party_invite` event in the room, and perform key validity checking if required by the event.
 *
 * @param roomIDOrAlias The room identifier or alias to join.
 * @param serverName The servers to attempt to join the room through. One of the servers must be participating in the room.
 * @returns The room has been joined.
 *
 * The joined room ID must be returned in the `room_id` field.
 */
export async function joinRoom(
    token: string,
    roomIDOrAlias: string,
    serverName?: string[],
    parameters?: {
        /** A signature of an `m.third_party_invite` token to prove that this user owns a third party identity which has been invited to the room. */
        third_party_signed?: {
            /** The Matrix ID of the invitee. */
            mxid: string;
            /** The Matrix ID of the user who issued the invite. */
            sender: string;
            /** A signatures object containing a signature of the entire signed object.  */
            signatures: Signatures;
            /** The state key of the m.third_party_invite event. */
            token: string;
        };
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The joined room ID. */
          room_id: string;
      }>
    | RequestError<RateLimitError>
> {
    const url = generateQueriedURL(join(baseURL, endpoints.joinRoomByIDOrAlias(roomIDOrAlias)), { serverName });
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `You do not have permission to join the room.
A meaningful \`errcode\` and description error text will be returned.
Example reasons for rejection are:
- The room is invite-only and the user was not invited.
- The user has been banned from the room.`,
                await response.json()
            );
        case 429:
            try {
                return await handleRateLimitRetry(response, joinRoom, [token, roomIDOrAlias, serverName, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Joined Rooms
 * Lists the user's current rooms.
 *
 * ### Implementation Notes
 * This API returns a list of the user's current rooms.
 *
 * @returns A list of the rooms the user is in.
 */
export async function getJoinedRooms(
    token: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The ID of each room in which the user has `joined` membership. */
          joined_rooms: string[];
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.joinedRooms);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Ban
 * Ban a user in the room.
 *
 * ### Implementation Notes
 * Ban a user in the room.
 * If the user is currently in the room, also kick them.
 *
 * When a user is banned from a room, they may not join it or be invited to it until they are unbanned.
 *
 * The caller must have the required power level in order to perform this operation.
 *
 * @param roomID The room identifier (not alias) from which the user should be banned.
 * @returns The user has been kicked and banned from the room.
 */
export async function ban(
    token: string,
    roomID: string,
    parameters: {
        /**
         * The `reason` the user has been banned.
         * This will be supplied as the reason on the target's updated `m.room.member`_ event.
         */
        reason?: string;
        /** The fully qualified user ID of the user being banned. */
        user_id: string;
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<APIError>> {
    const url = join(baseURL, endpoints.ban(roomID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `You do not have permission to ban the user from the room.
A meaningful errcode and description error text will be returned.
Example reasons for rejections are:
- The banner is not currently in the room.
- The banner's power level is insufficient to ban users from the room.`,
                await response.json()
            );
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Forget Room
 * Stop the requesting user remembering about a particular room.
 *
 * ### Implementation Notes
 * This API stops a user remembering about a particular room.
 *
 * In general, history is a first class citizen in Matrix.
 * After this API is called, however, a user will no longer be able to retrieve history for this room.
 * If all users on a homeserver forget a room, the room is eligible for deletion from that homeserver.
 *
 * If the user is currently joined to the room, they must leave the room before calling this API.
 *
 * @param roomID The room identifier to forget.
 * @returns The room has been forgotten.
 */
export async function forgetRoom(
    token: string,
    roomID: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.forgetRoom(roomID));
    const requestInit = generateRequestInit("POST", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(url, response, requestInit, "The user has not left the room", await response.json());
        case 429:
            try {
                return await handleRateLimitRetry(response, forgetRoom, [token, roomID, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Invite By 3PID
 * Invite a user to participate in a particular room.
 *
 * ### Implementation Notes
 * .. _invite-by-third-party-id-endpoint:
 *
 * Note that there are two forms of this API, which are documented separately.
 * This version of the API does not require that the inviter know the Matrix identifier of the invitee, and instead relies on third party identifiers.
 * The homeserver uses an identity server to perform the mapping from third party identifier to a Matrix identifier.
 * The other is documented in the `joining rooms section`_.
 *
 * This API invites a user to participate in a particular room.
 * They do not start participating in the room until they actually join the room.
 *
 * Only users currently in a particular room can invite other users to join that room.
 *
 * If the identity server did know the Matrix user identifier for the third party identifier, the homeserver will append a `m.room.member` event to the room.
 *
 * If the identity server does not know a Matrix user identifier for the passed third party identifier, the homeserver will issue an invitation which can be accepted upon providing proof of ownership of the third party identifier.
 * This is achieved by the identity server generating a token, which it gives to the inviting homeserver.
 * The homeserver will add an `m.room.third_party_invite` event into the graph for the room, containing that token.
 *
 * When the invitee binds the invited third party identifier to a Matrix user ID, the identity server will give the user a list of pending invitations, each containing:
 * - The room ID to which they were invited
 * - The token given to the homeserver
 * - A signature of the token, signed with the identity server's private key
 * - The matrix user ID who invited them to the room
 *
 * If a token is requested from the identity server, the homeserver will append a `m.room.third_party_invite` event to the room.
 *
 * .. joining rooms section: `invite-by-user-id-endpoint`
 *
 * @param roomID The room identifier (not alias) to which to invite the user.
 * @returns The user has been invited to join the room.
 */
export async function inviteBy3PID(
    token: string,
    roomID: string,
    parameters: {
        /** The invitee's third party identifier. */
        address: string;
        /**
         * An access token previously registered with the identity server.
         * Servers can treat this as optional to distinguish between r0.5-compatible clients and this specification version.
         */
        id_access_token: string;
        /** The hostname+port of the identity server which should be used for third party identifier lookups. */
        id_server: string;
        /** The kind of address being passed in the address field, for example `email`. */
        medium: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.invite(roomID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `You do not have permission to invite the user to the room.
A meaningful \`errcode\` and description error text will be returned.
Example reasons for rejections are:
- The invitee has been banned from the room.
- The invitee is already a member of the room.
- The inviter is not currently in the room.
- The inviter's power level is insufficient to invite users to the room.`,
                await response.json()
            );
        case 429:
            try {
                return await handleRateLimitRetry(response, inviteBy3PID, [token, roomID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Invite User
 * Invite a user to participate in a particular room.
 *
 * ### Implementation Notes
 * .. _invite-by-user-id-endpoint:
 *
 * Note that there are two forms of this API, which are documented separately.
 * This version of the API requires that the inviter knows the Matrix identifier of the invitee.
 * The other is documented in the `third party invites section`_.
 *
 * This API invites a user to participate in a particular room.
 * They do not start participating in the room until they actually join the room.
 *
 * Only users currently in a particular room can invite other users to join that room.
 *
 * If the user was invited to the room, the homeserver will append a `m.room.member` event to the room.
 *
 * .. third party invites section: `invite-by-third-party-id-endpoint`
 *
 * @param roomID The room identifier (not alias) to which to invite the user.
 * @returns The user has been invited to join the room.
 */
export async function inviteUser(
    token: string,
    roomID: string,
    parameters: {
        /** The fully qualified user ID of the invitee. */
        user_id: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.invite(roomID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(
                url,
                response,
                requestInit,
                `The request is invalid.
A meaningful \`errcode\` and description error text will be returned.
Example reasons for rejection include:
- The request body is malformed (\`errcode\` set to \`M_BAD_JSON\` or \`M_NOT_JSON\`).
- One or more users being invited to the room are residents of a homeserver which does not support the requested room version. The \`errcode\` will be \`M_UNSUPPORTED_ROOM_VERSION\` in these cases.`,
                await response.json()
            );
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `You do not have permission to invite the user to the room.
A meaningful \`errcode\` and description error text will be returned.
Example reasons for rejections are:
- The invitee has been banned from the room.
- The invitee is already a member of the room.
- The inviter is not currently in the room.
- The inviter's power level is insufficient to invite users to the room.`,
                await response.json()
            );
        case 429:
            try {
                return await handleRateLimitRetry(response, inviteUser, [token, roomID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Join Room By ID
 * Start the requesting user participating in a particular room.
 *
 * ### Implementation Notes
 * Note that this API requires a room ID, not alias.
 * `/join/{roomIdOrAlias}` exists if you have a room alias.
 *
 * This API starts a user participating in a particular room, if that user is allowed to participate in that room.
 * After this call, the client is allowed to see all current state events in the room, and all subsequent events associated with the room until the user leaves the room.
 *
 * After a user has joined a room, the room will appear as an entry in the response of the |/initialSync| and |/sync| APIs.
 *
 * If a `third_party_signed` was supplied, the homeserver must verify that it matches a pending `m.room.third_party_invite` event in the room, and perform key validity checking if required by the event.
 *
 * @param roomID The room identifier (not alias) to join.
 * @returns The room has been joined.
 *
 * The joined room ID must be returned in the `room_id` field.
 */
export async function joinRoomByID(
    token: string,
    roomID: string,
    parameters?: {
        /** A signature of an `m.third_party_invite` token to prove that this user owns a third party identity which has been invited to the room. */
        third_party_signed?: {
            /** The Matrix ID of the invitee. */
            mxid: string;
            /** The Matrix ID of the user who issued the invite. */
            sender: string;
            /** A signatures object containing a signature of the entire signed object.  */
            signatures: Signatures;
            /** The state key of the m.third_party_invite event. */
            token: string;
        };
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The joined room ID. */
          room_id: string;
      }>
    | RequestError<RateLimitError>
> {
    const url = join(baseURL, endpoints.joinRoomByID(roomID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `You do not have permission to join the room.
A meaningful \`errcode\` and description error text will be returned.
Example reasons for rejection are:
- The room is invite-only and the user was not invited.
- The user has been banned from the room.`,
                await response.json()
            );
        case 429:
            try {
                return await handleRateLimitRetry(response, joinRoomByID, [token, roomID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Kick
 * Kick a user from the room.
 *
 * ### Implementation Notes
 * Kick a user from the room.
 *
 * The caller must have the required power level in order to perform this operation.
 *
 * Kicking a user adjusts the target member's membership state to be `leave` with an optional `reason`.
 * Like with other membership changes, a user can directly adjust the target member's state by making a request to `/rooms//state/m.room.member/`.
 *
 * @param roomID The room identifier (not alias) from which the user should be kicked.
 * @returns The user has been kicked from the room.
 */
export async function kick(
    token: string,
    roomID: string,
    parameters: {
        /**
         * The reason the user has been kicked.
         * This will be supplied as the `reason` on the target's updated `m.room.member`_ event.
         */
        reason?: string;
        /** The fully qualified user ID of the user being kicked. */
        user_id: string;
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<APIError>> {
    const url = join(baseURL, endpoints.kick(roomID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `You do not have permission to kick the user from the room.
A meaningful \`errcode\` and description error text will be returned.
Example reasons for rejections are:
- The kicker is not currently in the room.
- The kickee is not currently in the room.
- The kicker's power level is insufficient to kick users from the room.`,
                await response.json()
            );
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Leave Room
 * Stop the requesting user participating in a particular room.
 *
 * ### Implementation Notes
 * This API stops a user participating in a particular room.
 *
 * If the user was already in the room, they will no longer be able to see new events in the room.
 * If the room requires an invite to join, they will need to be re-invited before they can re-join.
 *
 * If the user was invited to the room, but had not joined, this call serves to reject the invite.
 *
 * The user will still be allowed to retrieve history from the room which they were previously allowed to see.
 *
 * @param roomID The room identifier to leave.
 * @returns The room has been left.
 */
export async function leaveRoom(
    token: string,
    roomID: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.joinRoomByID(roomID));
    const requestInit = generateRequestInit("POST", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, leaveRoom, [token, roomID, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Unban
 * Unban a user from the room.
 *
 * ### Implementation Notes
 * Unban a user from the room.
 * This allows them to be invited to the room, and join if they would otherwise be allowed to join according to its join rules.
 *
 * The caller must have the required power level in order to perform this operation.
 *
 * @param roomID The room identifier (not alias) from which the user should be unbanned.
 * @returns The user has been unbanned from the room.
 */
export async function unban(
    token: string,
    roomID: string,
    parameters: {
        /** The fully qualified user ID of the user being unbanned. */
        user_id: string;
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<APIError>> {
    const url = join(baseURL, endpoints.unban(roomID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `You do not have permission to unban the user from the room.
A meaningful \`errcode\` and description error text will be returned.
Example reasons for rejections are:
- The unbanner's power level is insufficient to unban users from the room.`,
                await response.json()
            );
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

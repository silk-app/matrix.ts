import { defaultBaseURL, endpoints, errorMessages } from "../constants.ts";
import { join } from "../deps.ts";
import { RequestError, UnauthorizedError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { generateAPIResponse, generateRequestInit } from "../utils.ts";

/** ## Delete Devices
 * Bulk deletion of devices
 *
 * ### Implementation Notes
 * This API endpoint uses the `User-Interactive Authentication API`_.
 *
 * Deletes the given devices, and invalidates any access token associated with them.
 *
 * @returns The devices were successfully removed, or had been removed previously.
 */
export async function deleteDevices(
    token: string,
    parameters: {
        /** Additional authentication information for the user-interactive authentication API. */
        auth?: {
            /** The value of the session key given by the homeserver. */
            session?: string;
            /** The login type that the client is attempting to complete. */
            type: string;
        };
        /** The list of device IDs to delete. */
        devices: string[];
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<UnauthorizedError>> {
    const url = join(baseURL, endpoints.deleteDevices);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 401:
            return new RequestError(url, response, requestInit, "The homeserver requires additional authentication information.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Devices
 * List registered devices for the current user
 *
 * ### Implementation Notes
 * Gets information about all devices for the current user.
 *
 * @returns Device information
 */
export async function getDevices(
    token: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** A list of all registered devices for this user. */
          devices?: {
              /** Identifier of this device. */
              device_id: string;
              /**
               * Display name set by the user for this device.
               * Absent if no name has been set.
               */
              display_name?: string;
              /**
               * The IP address where this device was last seen.
               * (May be a few minutes out of date, for efficiency reasons).
               */
              last_seen_ip?: string;
              /**
               * The timestamp (in milliseconds since the unix epoch) when this devices was last seen.
               * (May be a few minutes out of date, for efficiency reasons).
               */
              last_seen_ts?: number;
          }[];
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.devices);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Delete Device
 * Delete a Device
 *
 * ### Implementation Notes
 * This API endpoint uses the `User-Interactive Authentication API`_.
 *
 * Deletes the given device, and invalidates any access token associated with it.
 *
 * @param deviceID the device to delete.
 * @returns The device was successfully removed, or had been removed previously.
 */
export async function deleteDevice(
    token: string,
    deviceID: string,
    parameters: {
        /** Additional authentication information for the user-interactive authentication API. */
        auth?: {
            // deno-lint-ignore no-explicit-any (Example value contains non-documented key `example_credential`)
            [key: string]: any;
            /** The value of the session key given by the homeserver. */
            session?: string;
            /** The login type that the client is attempting to complete. */
            type: string;
        };
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<UnauthorizedError>> {
    const url = join(baseURL, endpoints.device(deviceID));
    const requestInit = generateRequestInit("DELETE", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 401:
            return new RequestError(url, response, requestInit, "The homeserver requires additional authentication information", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Device
 * Get a single device
 *
 * ### Implementation Notes
 * Gets information on a single device, by device id.
 *
 * @param deviceID The device to retrieve.
 * @returns Device information
 */
export async function getDevice(
    token: string,
    deviceID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** Identifier of this device. */
          device_id: string;
          /**
           * Display name set by the user for this device.
           * Absent if no name has been set.
           */
          display_name?: string;
          /**
           * The IP address where this device was last seen.
           * (May be a few minutes out of date, for efficiency reasons).
           */
          last_seen_ip?: string;
          /**
           * The timestamp (in milliseconds since the unix epoch) when this devices was last seen.
           * (May be a few minutes out of date, for efficiency reasons).
           */
          last_seen_ts?: number;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.device(deviceID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "The current user has no device with the given ID.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Update Device
 * Update a device
 *
 * ### Implementation Notes
 * Updates the metadata on the given device.
 *
 * @param deviceID The device to update.
 * @returns The device was successfully updated.
 */
export async function updateDevice(
    token: string,
    deviceID: string,
    parameters: {
        /**
         * The new display name for this device.
         * If not given, the display name is unchanged.
         */
        display_name?: string;
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.device(deviceID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "The current user has no device with the given ID.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

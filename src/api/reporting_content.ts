import { RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit } from "../utils.ts";
import { defaultBaseURL, endpoints, errorMessages } from "../constants.ts";

/** ## Report Content
 * Reports an event as inappropriate.
 *
 * ### Implementation Notes
 * Reports an event as inappropriate to the server, which may then notify the appropriate people.
 *
 * @param roomID The room in which the event being reported is located.
 * @param eventID The event to report.
 * @returns The event has been reported successfully.
 */
export async function reportContent(
    token: string,
    roomID: string,
    eventID: string,
    parameters: {
        /**
         * The reason the content is being reported.
         * May be blank.
         */
        reason: string;
        /** The score to rate this content as where -100 is most offensive and 0 is inoffensive. */
        score: number;
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.reportContent(roomID, eventID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

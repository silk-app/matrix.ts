import { APIError, APIErrorCode, APIErrorExcept, RateLimitError, RequestError, UnauthorizedError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateQueriedURL, generateRequestInit, handleRateLimitRetry } from "../utils.ts";
import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";

/** ## Get Account 3PIDs
 * Gets a list of a user's third party identifiers
 *
 * ### Implementation Notes
 * Gets a list of the third party identifiers that the homeserver has associated with the user's account.
 *
 * This is not the same as the list of third party identifiers bound to the user's Matrix ID in identity servers.
 *
 * Identifiers in this list may be used by the homeserver as, for example, identifiers that it will accept to reset the user's account password.
 *
 * @returns The lookup was successful.
 */
export async function getAccount3PIDs(
    token: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          threepids?: {
              /** The timestamp, in milliseconds, when the homeserver associated the third party identifier with the user */
              added_at: number;
              /** The third party identifier address */
              address: string;
              /** The medium of the third party identifier. */
              medium: "email" | "msisdn";
              /** The timestamp, in milliseconds, when the identifier was validated by the identity server */
              validated_at: number;
          }[];
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.account3PIDs);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Post 3PIDs
 * Adds contact information to the user's account.
 *
 * ### Implementation Notes
 * Adds contact information to the user's account.
 *
 * @deprecated This endpoint is deprecated in favour of the more specific `/3pid/add` and `/3pid/bind` endpoints.
 *
 * .. Note:: Previously this endpoint supported a `bind` parameter.
 * This parameter has been removed, making this endpoint behave as though it was `false`.
 * This results in this endpoint being an equivalent to `/3pid/bind` rather than dual-purpose.
 *
 * @returns The addition was successful.
 */
export async function post3PIDs(
    token: string,
    parameters: {
        /** The third party credentials to associate with the account. */
        three_pid_creds: {
            /** The client secret used in the session with the identity server. */
            client_secret: string;
            /** An access token previously registered with the identity server. Servers can treat this as optional to distinguish between r0.5-compatible clients and this specification version. */
            id_access_token: string;
            /** The identity server to use. */
            id_server: string;
            /** The session identifier given by the identity server. */
            sid: string;
        };
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<APIError>> {
    const url = join(baseURL, endpoints.account3PIDs);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(url, response, requestInit, "The credentials could not be verified with the identity server.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Bind 3PID
 * Binds a 3PID to the user's account through an Identity Service.
 *
 * ### Implementation Notes
 * Binds a 3PID to the user's account through the specified identity server.
 *
 * Homeservers should not prevent this request from succeeding if another user has bound the 3PID.
 * Homeservers should simply proxy any errors received by the identity server to the caller.
 *
 * Homeservers should track successful binds so they can be unbound later.
 *
 * @returns The addition was successful.
 */
export async function bind3PID(
    token: string,
    parameters: {
        /** The client secret used in the session with the identity server. */
        client_secret: string;
        /** An access token previously registered with the identity server. */
        id_access_token: string;
        /** The identity server to use. */
        id_server: string;
        /** The session identifier given by the identity server */
        sid: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.bind3PID);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, bind3PID, [token, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Delete 3PID From Account
 * Deletes a third party identifier from the user's account
 *
 * ### Implementation Notes
 * Removes a third party identifier from the user's account.
 * This might not cause an unbind of the identifier from the identity server.
 *
 * Unlike other endpoints, this endpoint does not take an id_access_token parameter because the homeserver is expected to sign the request to the identity server instead.
 *
 * @returns The homeserver has disassociated the third party identifier from the user.
 */
export async function delete3PIDFromAccount(
    token: string,
    parameters: {
        /** The third party address being removed. */
        address: string;
        /**
         * The identity server to unbind from.
         * If not provided, the homeserver MUST use the `id_server` the identifier was added through.
         * If the homeserver does not know the original `id_server`, it MUST return a `id_server_unbind_result` of `no-support`.
         */
        id_server?: string;
        /** The medium of the third party identifier being removed. */
        medium: "email" | "msisdn";
    },
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * An indicator as to whether or not the homeserver was able to unbind the 3PID from the identity server.
           * `success` indicates that the indentity server has unbound the identifier whereas no-support indicates that the identity server refuses to support the request or the homeserver was not able to determine an identity server to unbind from.
           */
          id_server_unbind_result: "no-support" | "success";
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.delete3PIDFromAccount);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Unbind 3PID From Account
 * Removes a user's third party identifier from an identity server
 *
 * ### Implementation Notes
 * Removes a user's third party identifier from the provided identity server without removing it from the homeserver.
 *
 * Unlike other endpoints, this endpoint does not take an `id_access_token` parameter because the homeserver is expected to sign the request to the identity server instead.
 *
 * @returns The identity server has disassociated the third party identifier from the user.
 */
export async function unbind3PIDFromAccount(
    token: string,
    parameters: {
        /** The third party address being removed. */
        address: string;
        /**
         * The identity server to unbind from.
         * If not provided, the homeserver MUST use the `id_server` the identifier was added through.
         * If the homeserver does not know the original `id_server`, it MUST return a `id_server_unbind_result` of `no-support`.
         */
        id_server?: string;
        /** The medium of the third party identifier being removed. */
        medium: "email" | "msisdn";
    },
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * An indicator as to whether or not the homeserver was able to unbind the 3PID from the identity server.
           * `success` indicates that the indentity server has unbound the identifier whereas no-support indicates that the identity server refuses to support the request or the homeserver was not able to determine an identity server to unbind from.
           */
          id_server_unbind_result: "no-support" | "success";
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.unbind3PIDFromAccount);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Deactivate Account
 * Deactivate a user's account
 *
 * ### Implementation Notes
 * Deactivate the user's account, removing all ability for the user to login again.
 *
 * This API endpoint uses the `User-Interactive Authentication API`_.
 *
 * An access token should be submitted to this endpoint if the client has an active session.
 *
 * The homeserver may change the flows available depending on whether a valid access token is provided.
 *
 * Unlike other endpoints, this endpoint does not take an `id_access_token` parameter because the homeserver is expected to sign the request to the identity server instead.
 *
 * @returns The account has been deactivated.
 */
export async function deactivateAccount(
    token: string,
    parameters: {
        /** Additional authentication information for the user-interactive authentication API. */
        auth?: {
            /** The value of the session key given by the homeserver. */
            session?: string;
            /** The login type that the client is attempting to complete. */
            type: string;
        };
        /**
         * The identity server to unbind all of the user's 3PIDs from.
         * If not provided, the homeserver MUST use the `id_server` that was originally use to bind each identifier.
         * If the homeserver does not know which id_server that was, it must return an `id_server_unbind_result` of `no-support`.
         */
        id_server?: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** An indicator as to whether or not the homeserver was able to unbind the user's 3PIDs from the identity server(s).
           * `success` indicates that all identifiers have been unbound from the identity server while `no-support` indicates
           * that one or more identifiers failed to unbind due to the identity server refusing the request or the homeserver
           * being unable to determine an identity server to unbind from.
           * This must be `success` if the homeserver has no identifiers to unbind for the user.
           */
          id_server_unbind_result: "no-support" | "success";
      }>
    | RequestError<RateLimitError | UnauthorizedError>
> {
    const url = join(baseURL, endpoints.deactivateAccount);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 401:
            return new RequestError(url, response, requestInit, "The homeserver requires additional authentication information.", await response.json());
        case 429:
            try {
                return await handleRateLimitRetry(response, deactivateAccount, [token, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Change Password
 * Changes a user's password
 *
 * ### Implementation Notes
 * Changes the password for an account on this homeserver.
 *
 * This API endpoint uses the `User-Interactive Authentication API`_ to ensure the user changing the password is actually the owner of the account.
 *
 * An access token should be submitted to this endpoint if the client has an active session.
 *
 * The homeserver may change the flows available depending on whether a valid access token is provided.
 * The homeserver SHOULD NOT revoke the access token provided in the request. Whether other access tokens for the user are revoked depends on the request parameters.
 *
 * @returns The password has been changed.
 */
export async function changePassword(
    token: string,
    parameters: {
        /** Additional authentication information for the user-interactive authentication API. */
        auth?: {
            /** The value of the session key given by the homeserver. */
            session?: string;
            /** The login type that the client is attempting to complete. */
            type: string;
        };
        /**
         * Whether the user's other access tokens, and their associated devices, should be revoked if the request succeeds.
         * Defaults to true.
         *
         * When `false`, the server can still take advantage of `the soft logout method`_ for the user's remaining devices.
         */
        logout_devices?: boolean;
        /** The new password for the account. */
        new_password: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<UnauthorizedError | RateLimitError>> {
    const url = join(baseURL, endpoints.changePassword);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 401:
            return new RequestError(url, response, requestInit, "The homeserver requires additional authentication information.", await response.json());
        case 429:
            try {
                return await handleRateLimitRetry(response, changePassword, [token, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Token Owner
 * Gets information about the owner of an access token.
 *
 * ### Implementation Notes
 * Gets information about the owner of a given access token.
 *
 * Note that, as with the rest of the Client-Server API, Application Services may masquerade as users within their namespace by giving a `user_id` query parameter.
 * In this situation, the server should verify that the given `user_id` is registered by the appservice, and return it in the response body.
 *
 * @returns The token belongs to a known user.
 */
export async function getTokenOwner(
    token: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The user id that owns the access token. */
          user_id: string;
      }>
    | RequestError<RateLimitError | APIErrorExcept<"M_LIMIT_EXCEEDED">>
> {
    const url = join(baseURL, endpoints.getTokenOwner);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 401:
            return new RequestError(url, response, requestInit, "The token is not recognised", await response.json());
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                "The appservice cannot masquerade as the user or has not registered them.",
                await response.json()
            );
        case 429:
            try {
                return await handleRateLimitRetry(response, getTokenOwner, [token, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get User Profile
 * Get this user's profile information.
 *
 * ### Implementation Notes
 * Get the combined profile information for this user.
 * This API may be used to fetch the user's own profile information or other users; either locally or on remote homeservers.
 * This API may return keys which are not limited to `displayname` or `avatar_url`.
 *
 * @param userID The user whose profile information to get.
 * @returns The avatar URL for this user.
 */
export async function getUserProfile(
    userID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The user's avatar URL if they have set one, otherwise not present. */
          avatar_url?: string;
          /** The user's display name if they have set one, otherwise not present. */
          displayname?: string;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.userProfile(userID));
    const requestInit = generateRequestInit("GET");
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "There is no profile information for this user or this user does not exist.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Avatar URL
 * Get the user's avatar URL.
 *
 * ### Implementation Notes
 * Get the user's avatar URL.
 * This API may be used to fetch the user's own avatar URL or to query the URL of other users; either locally or on remote homeservers.
 *
 * @param userID The user whose avatar URL to get.
 * @returns The avatar URL for this user.
 */
export async function getAvatarURL(
    userID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The user's avatar URL if they have set one, otherwise not present. */
          avatar_url?: string;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.avatarURL(userID));
    const requestInit = generateRequestInit("GET");
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "There is no avatar URL for this user or this user does not exist.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Avatar URL
 * Set the user's avatar URL.
 *
 * ### Implementation Notes
 * This API sets the given user's avatar URL.
 * You must have permission to set this user's avatar URL, e.g. you need to have their `access_token`.
 *
 * @param userID The user whose avatar URL to set.
 * @returns The avatar URL was set.
 */
export async function setAvatarURL(
    token: string,
    userID: string,
    parameters: {
        /** The new avatar URL for this user. */
        avatar_url?: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.avatarURL(userID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, setAvatarURL, [token, userID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Display Name
 * Get the user's display name.
 *
 * ### Implementation Notes
 * Get the user's display name.
 * This API may be used to fetch the user's own displayname or to query the name of other users; either locally or on remote homeservers.
 *
 * @param userID The user whose display name to get.
 * @returns The display name for this user.
 */
export async function getDisplayName(
    userID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The user's display name if they have set one, otherwise not present. */
          displayname?: string;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.displayName(userID));
    const requestInit = generateRequestInit("GET");
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 404:
            return new RequestError(url, response, requestInit, "There is no display name for this user or this user does not exist.");
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Display Name
 * Set the user's display name.
 *
 * ### Implementation Notes
 * This API sets the given user's display name.
 * You must have permission to set this user's display name, e.g. you need to have their `access_token`.
 *
 * @param userID The user whose display name to set.
 * @returns The display name was set.
 */
export async function setDisplayName(
    token: string,
    userID: string,
    parameters: {
        /** The new display name for this user. */
        displayName?: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.displayName(userID));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, setDisplayName, [token, userID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Register
 * Register for an account on this homeserver.
 *
 * ### Implementation Notes
 * This API endpoint uses the `User-Interactive Authentication API`_, except in the cases where a guest account is being registered.
 *
 * Register for an account on this homeserver.
 *
 * There are two kinds of user account:
 * - `user` accounts. These accounts may use the full API described in this specification.
 * - `guest` accounts. These accounts may have limited permissions and may not be supported by all servers.
 *
 * If registration is successful, this endpoint will issue an access token the client can use to authorize itself in subsequent requests.
 *
 * If the client does not supply a `device_id`, the server must auto-generate one.
 *
 * The server SHOULD register an account with a User ID based on the `username` provided, if any.
 * Note that the grammar of Matrix User ID localparts is restricted, so the server MUST either map the provided `username` onto a `user_id` in a logical manner, or reject `username`\s which do not comply to the grammar, with `M_INVALID_USERNAME`.
 *
 * Matrix clients MUST NOT assume that localpart of the registered `user_id` matches the provided `username`.
 *
 * The returned access token must be associated with the `device_id` supplied by the client or generated by the server.
 * The server may invalidate any access token previously associated with that device.
 * See `Relationship between access tokens and devices`_.
 *
 * When registering a guest account, all parameters in the request body with the exception of `initial_device_display_name` MUST BE ignored by the server.
 * The server MUST pick a `device_id` for the account regardless of input.
 *
 * Any user ID returned by this API must conform to the grammar given in the `Matrix specification`_.
 *
 * @param kind The kind of account to register. Defaults to `user`.
 * @returns The account has been registered.
 */
export async function register(
    kind?: "user" | "guest",
    parameters?: {
        /**
         * Additional authentication information for the user-interactive authentication API.
         * Note that this information is not used to define how the registered user should be authenticated, but is instead used to authenticate the `register` call itself.
         */
        auth?: {
            /** The value of the session key given by the homeserver. */
            session?: string;
            /** The login type that the client is attempting to complete. */
            type: string;
        };
        /**
         * ID of the client device. If this does not correspond to a known client device, a new device will be created.
         * The server will auto-generate a device_id if this is not specified.
         */
        device_id?: string;
        /**
         * If true, an `access_token` and `device_id` should not be returned from this call, therefore preventing an automatic login.
         * Defaults to false.
         */
        inhibit_login?: boolean;
        /**
         * A display name to assign to the newly-created device.
         * Ignored if `device_id` corresponds to a known device.
         */
        initial_device_display_name?: string;
        /** The desired password for the account. */
        password?: string;
        /**
         * The basis for the localpart of the desired Matrix ID.
         * If omitted, the homeserver MUST generate a Matrix ID local part.
         */
        username?: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * An access token for the account.
           * This access token can then be used to authorize other requests.
           * Required if the `inhibit_login` option is false.
           */
          access_token?: string;
          /**
           * ID of the registered device.
           * Will be the same as the corresponding parameter in the request, if one was specified.
           * Required if the inhibit_login option is false.
           */
          device_id?: string;
          /**
           * The server_name of the homeserver on which the account has been registered.
           *
           * @deprecated Clients should extract the server_name from `user_id` (by splitting at the first colon) if they require it. Note also that `homeserver` is not spelt this way.
           */
          home_server?: string;
          /**
           * The fully-qualified Matrix user ID (MXID) that has been registered.
           *
           * Any user ID returned by this API must conform to the grammar given in the `Matrix specification`_.
           */
          user_id: string;
      }>
    | RequestError<UnauthorizedError | RateLimitError>
> {
    const url = generateQueriedURL(join(baseURL, endpoints.register), { kind });
    const requestInit = generateRequestInit("POST", undefined, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(
                url,
                response,
                requestInit,
                `Part of the request was invalid.
This may include one of the following error codes:
- \`M_USER_IN_USE\` : The desired user ID is already taken.
- \`M_INVALID_USERNAME\` : The desired user ID is not a valid user name.
- \`M_EXCLUSIVE\` : The desired user ID is in the exclusive namespace claimed by an application service.
These errors may be returned at any stage of the registration process, including after authentication if the requested user ID was registered whilst the client was performing authentication.

Homeservers MUST perform the relevant checks and return these codes before performing User-Interactive Authentication, although they may also return them after authentication is completed if, for example, the requested user ID was registered whilst the client was performing authentication.`,
                await response.json()
            );
        case 401:
            return new RequestError(url, response, requestInit, "The homeserver requires additional authentication information.", await response.json());
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `The homeserver does not permit registering the account.
This response can be used to identify that a particular \`kind\` of account is not allowed, or that registration is generally not supported by the homeserver.`,
                await response.json()
            );
        case 429:
            try {
                return await handleRateLimitRetry(response, register, [kind, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Check Username Availability
 * Checks to see if a username is available on the server.
 *
 * ### Implementation Notes
 * Checks to see if a username is available, and valid, for the server.
 *
 * The server should check to ensure that, at the time of the request, the username requested is available for use.
 * This includes verifying that an application service has not claimed the username and that the username fits the server's desired requirements (for example, a server could dictate that it does not permit usernames with underscores).
 *
 * Matrix clients may wish to use this API prior to attempting registration, however the clients must also be aware that using this API does not normally reserve the username.
 * This can mean that the username becomes unavailable between checking its availability and attempting to register it.
 *
 * @param username The username to check the availability of.
 * @returns The username is available
 */
export async function checkUsernameAvailability(
    username: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * A flag to indicate that the username is available.
           * This should always be true when the server replies with 200 OK.
           */
          available?: boolean;
      }>
    | RequestError<RateLimitError>
> {
    const url = join(baseURL, endpoints.checkUsernameAvailability);
    const requestInit = generateRequestInit("GET", undefined, username);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(
                url,
                response,
                requestInit,
                `Part of the request was invalid or the username is not available.
This may include one of the following error codes:
- \`M_USER_IN_USE\` : The desired username is already taken.
- \`M_INVALID_USERNAME\` : The desired username is not a valid user name.
- \`M_EXCLUSIVE\` : The desired username is in the exclusive namespace claimed by an application service.`,
                await response.json()
            );
        case 429:
            try {
                return await handleRateLimitRetry(response, checkUsernameAvailability, [username, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Account Data
 * Get some account_data for the user.
 *
 * ### Implementation Notes
 * Get some account_data for the client.
 * This config is only visible to the user that set the account_data.
 *
 * @param userID The ID of the user to get account_data for. The access token must be authorized to make requests for this user ID.
 * @param type The event type of the account_data to get. Custom types should be namespaced to avoid clashes.
 * @returns The account data content for the given type.
 */
export async function getAccountData(token: string, userID: string, type: string, baseURL: string = defaultBaseURL): Promise<APIResponse<any> | RequestError> {
    const url = join(baseURL, endpoints.accountData(userID, type));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Account Data
 * Set some account_data for the user.
 *
 * ### Implementation Notes
 * Set some account_data for the client. This config is only visible to the user that set the account_data.
 * The config will be synced to clients in the top-level `account_data`.
 *
 * @param userID The ID of the user to set account_data for. The access token must be authorized to make requests for this user ID.
 * @param type The event type of the account_data to set. Custom types should be namespaced to avoid clashes.
 * @returns The account_data was successfully added.
 */
export async function setAccountData(
    token: string,
    userID: string,
    type: string,
    parameters: any,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.accountData(userID, type));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Account Data Per Room
 * Get some account_data for the user.
 *
 * ### Implementation Notes
 * Get some account_data for the client on a given room.
 * This config is only visible to the user that set the account_data.
 *
 * @param userID The ID of the user to set account_data for. The access token must be authorized to make requests for this user ID.
 * @param roomID The ID of the room to get account_data for.
 * @param type The event type of the account_data to get. Custom types should be namespaced to avoid clashes.
 * @returns The account data content for the given type.
 */
export async function getAccountDataPerRoom(
    token: string,
    userID: string,
    roomID: string,
    type: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<any> | RequestError> {
    const url = join(baseURL, endpoints.accountDataPerRoom(userID, roomID, type));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Account Data Per Room
 * Set some account_data for the user.
 *
 * ### Implementation Notes
 * Set some account_data for the client on a given room.
 * This config is only visible to the user that set the account_data.
 * The config will be synced to clients in the per-room `account_data`.
 *
 * @param userID The ID of the user to set account_data for. The access token must be authorized to make requests for this user ID.
 * @param roomID The ID of the room to get account_data for.
 * @param type The event type of the account_data to get. Custom types should be namespaced to avoid clashes.
 * @returns The account_data was successfully added.
 */
export async function setAccountDataPerRoom(
    token: string,
    userID: string,
    roomID: string,
    type: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.accountDataPerRoom(userID, roomID, type));
    const requestInit = generateRequestInit("PUT", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Room Tags
 * List the tags for a room.
 *
 * ### Implementation Notes
 * List the tags set by a user on a room.
 *
 * @param userID The id of the user to get tags for. The access token must be authorized to make requests for this user ID.
 * @param roomID The ID of the room to get tags for.
 * @returns The list of tags for the user for the room.
 */
export async function getRoomTags(
    token: string,
    userID: string,
    roomID: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          tags?: any;
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.roomTags(userID, roomID));
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Delete Room Tag
 * Remove a tag from the room.
 *
 * ### Implementation Notes
 * Remove a tag from the room.
 *
 * @param userID The id of the user to remove a tag for. The access token must be authorized to make requests for this user ID.
 * @param roomID The ID of the room to remove a tag from.
 * @param tag The tag to remove.
 */
export async function deleteRoomTag(
    token: string,
    userID: string,
    roomID: string,
    tag: string,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.roomTag(userID, roomID, tag));
    const requestInit = generateRequestInit("DELETE", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Set Room Tag
 * Add a tag to a room.
 *
 * ### Implementation Notes
 * Add a tag to the room.
 *
 * @param userID The id of the user to add a tag for. The access token must be authorized to make requests for this user ID.
 * @param roomID The ID of the room to add a tag to.
 * @param tag The tag to add.
 * @returns The tag was successfully added.
 */
export async function setRoomTag(
    token: string,
    userID: string,
    roomID: string,
    tag: string,
    parameters: {
        /** A number in a range `[0,1]` describing a relative position of the room under the given tag. */
        order?: number;
    },
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError> {
    const url = join(baseURL, endpoints.roomTag(userID, roomID, tag));
    const requestInit = generateRequestInit("PUT", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Search User Directory
 * Searches the user directory.
 *
 * ### Implementation Notes
 * Performs a search for users.
 * The homeserver may determine which subset of users are searched, however the homeserver MUST at a minimum consider the users the requesting user shares a room with and those who reside in public rooms (known to the homeserver).
 * The search MUST consider local users to the homeserver, and SHOULD query remote users as part of the search.
 *
 * The search is performed case-insensitively on user IDs and display names preferably using a collation determined based upon the `Accept-Language` header provided in the request, if present.
 *
 * @returns The results of the search.
 */
export async function searchUserDirectory(
    token: string,
    parameters?: {
        /**
         * The maximum number of results to return.
         * Defaults to 10.
         */
        limit?: number;
        /** The term to search for */
        search_term: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** Indicates if the result list has been truncated by the limit. */
          limited: boolean;
          /** Ordered by rank and then whether or not profile info is available. */
          results: {
              /** The avatar url, as an MXC, if one exists. */
              avatar_url?: string;
              /** The display name of the user, if one exists. */
              display_name?: string;
              /** The user's matrix user ID. */
              user_id: string;
          }[];
      }>
    | RequestError<RateLimitError>
> {
    const url = join(baseURL, endpoints.searchUserDirectory);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, searchUserDirectory, [token, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

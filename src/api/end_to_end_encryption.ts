import { defaultBaseURL, endpoints, errorMessages } from "../constants.ts";
import { RequestError } from "../errors.ts";
import { APIResponse, Signatures } from "../types/mod.ts";
import { generateAPIResponse, generateQueriedURL, generateRequestInit } from "../utils.ts";
import { join } from "../deps.ts";

/** ## Get Keys Changes
 * Query users with recent device key updates.
 *
 * ### Implementation Notes
 * Gets a list of users who have updated their device identity keys since a previous sync token.
 *
 * The server should include in the results any users who:
 * - currently share a room with the calling user (ie, both users have membership state `join`); and
 * - added new device identity keys or removed an existing device with identity keys, between `from` and `to`.
 *
 * @param from
 *      The desired start point of the list.
 *      Should be the `next_batch` field from a response to an earlier call to |/sync|.
 *      Users who have not uploaded new device identity keys since this point, nor deleted existing devices with identity keys since then, will be excluded from the results.
 * @param to
 *      The desired end point of the list.
 *      Should be the `next_batch` field from a recent call to |/sync| - typically the most recent such call.
 *      This may be used by the server as a hint to check its caches are up to date.
 * @returns The list of users who updated their devices.
 */
export async function getKeysChanges(
    token: string,
    from: string,
    to: string,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The Matrix User IDs of all users who updated their device identity keys. */
          changed?: string[];
          /** The Matrix User IDs of all users who may have left all the end-to-end encrypted rooms they previously shared with the user. */
          left?: string[];
      }>
    | RequestError
> {
    const url = generateQueriedURL(join(baseURL, endpoints.keysChanges), { from, to });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Claim Keys
 * Claim one-time encryption keys
 *
 * ### Implementation Notes
 * Claims one-time keys for use in pre-key messages.
 *
 * @returns The claimed keys.
 */
export async function claimKeys(
    token: string,
    parameters: {
        /**
         * The keys to be claimed.
         * A map from user ID, to a map from device ID to algorithm name.
         */
        one_time_keys: {
            [userID: string]: {
                [deviceID: string]: string;
            };
        };
        /**
         * The time (in milliseconds) to wait when downloading keys from remote servers.
         * 10 seconds is the recommended default.
         */
        timeout?: number;
    },
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * If any remote homeservers could not be reached, they are recorded here.
           * The names of the properties are the names of the unreachable servers.
           *
           * If the homeserver could be reached, but the user or device was unknown, no failure is recorded.
           * Instead, the corresponding user or device is missing from the `one_time_keys` result.
           */
          failures?: {
              // deno-lint-ignore no-explicit-any (Type not documented)
              [homeserver: string]: any;
          };
          /**
           * One-time keys for the queried devices.
           * A map from user ID, to a map from devices to a map from `<algorithm>:<key_id>` to the key object.
           */
          one_time_keys: {
              [userID: string]: {
                  [deviceID: string]:
                      | string
                      | {
                            [keyID: string]: {
                                /** The unpadded Base64-encoded 32-byte Curve25519 public key. */
                                key: string;
                                /** Signatures of the key object. */
                                signatures: Signatures;
                            };
                        };
              };
          };
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.claimKeys);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Query Keys
 * Download device identity keys.
 *
 * ### Implementation Notes
 * Returns the current devices and identity keys for the given users.
 *
 * @returns The device information
 */
export async function queryKeys(
    token: string,
    parameters: {
        /**
         * The keys to be downloaded.
         * A map from user ID, to a list of device IDs, or to an empty list to indicate all devices for the corresponding user.
         */
        device_keys: {
            [userID: string]: string[];
        };
        /**
         * The time (in milliseconds) to wait when downloading keys from remote servers.
         * 10 seconds is the recommended default.
         */
        timeout?: number;
        /**
         * If the client is fetching keys as a result of a device update received in a sync request, this should be the 'since' token of that sync request, or any later sync token.
         * This allows the server to ensure its response contains the keys advertised by the notification in that sync.
         */
        token?: string;
    },
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * Information on the queried devices.
           * A map from user ID, to a map from device ID to device information.
           * For each device, the information returned will be the same as uploaded via `/keys/upload`, with the addition of an `unsigned` property.
           */
          device_keys?: {
              [userID: string]: {
                  [deviceID: string]: {
                      /**
                       * The ID of the user the device belongs to.
                       * Must match the user ID used when logging in.
                       */
                      user_id: string;
                      /**
                       * The ID of the device these keys belong to.
                       * Must match the device ID used when logging in.
                       */
                      device_id: string;
                      /** The encryption algorithms supported by this device. */
                      algorithms: string[];
                      /**
                       * Public identity keys.
                       * The names of the properties should be in the format `<algorithm>:<device_id>`.
                       * The keys themselves should be encoded as specified by the key algorithm.
                       */
                      keys: {
                          [keyID: string]: string;
                      };
                      /**
                       * Signatures for the device key object.
                       * A map from user ID, to a map from `<algorithm>:<device_id>` to the signature.
                       */
                      signatures: Signatures;
                      /** Additional data added to the device key information by intermediate servers, and not covered by the signatures. */
                      unsigned?: {
                          /** The display name which the user set on the device. */
                          device_display_name?: string;
                      };
                  };
              };
          };
          /**
           * If any remote homeservers could not be reached, they are recorded here.
           * The names of the properties are the names of the unreachable servers.
           *
           * If the homeserver could be reached, but the user or device was unknown, no failure is recorded.
           * Instead, the corresponding user or device is missing from the `one_time_keys` result.
           */
          failures?: {
              // deno-lint-ignore no-explicit-any (Type not documented)
              [homeserver: string]: any;
          };
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.queryKeys);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Upload Keys
 * Upload end-to-end encryption keys.
 *
 * ### Implementation Notes
 * Publishes end-to-end encryption keys for the device.
 *
 * @returns The provided keys were successfully uploaded.
 */
export async function uploadKeys(
    token: string,
    parameters: {
        /**
         * Identity keys for the device.
         * May be absent if no new identity keys are required.
         */
        device_keys?: {
            /**
             * The ID of the user the device belongs to.
             * Must match the user ID used when logging in.
             */
            user_id: string;
            /**
             * The ID of the device these keys belong to.
             * Must match the device ID used when logging in.
             */
            device_id: string;
            /** The encryption algorithms supported by this device. */
            algorithms: string[];
            /**
             * Public identity keys.
             * The names of the properties should be in the format `<algorithm>:<device_id>`.
             * The keys themselves should be encoded as specified by the key algorithm.
             */
            keys: {
                [keyID: string]: string;
            };
            /**
             * Signatures for the device key object.
             * A map from user ID, to a map from `<algorithm>:<device_id>` to the signature.
             */
            signatures: Signatures;
        };
        /**
         * One-time public keys for "pre-key" messages.
         * The names of the properties should be in the format `<algorithm>:<key_id>`.
         * The format of the key is determined by the key algorithm.
         *
         * May be absent if no new one-time keys are required.
         */
        one_time_keys: {
            [userID: string]: {
                [deviceID: string]:
                    | string
                    | {
                          [keyID: string]: {
                              /** The unpadded Base64-encoded 32-byte Curve25519 public key. */
                              key: string;
                              /** Signatures of the key object. */
                              signatures: Signatures;
                          };
                      };
            };
        };
    },
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** For each key algorithm, the number of unclaimed one-time keys of that type currently held on the server for this device. */
          one_time_key_counts: {
              [algorithm: string]: number;
          };
      }>
    | RequestError
> {
    const url = join(baseURL, endpoints.uploadKeys);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

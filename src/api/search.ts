import { APIError, RateLimitError, RequestError } from "../errors.ts";
import { APIResponse, RoomEvents, RoomStateEvents } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit, handleRateLimitRetry } from "../utils.ts";
import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";

/** ## Search
 * Perform a server-side search.
 *
 * ### Implementation Notes
 * Performs a full text search across different categories.
 *
 * @param nextBatch The point to return events from. If given, this should be a `next_batch` result from a previous call to this endpoint.
 * @returns Results of the search.
 */
export async function search<include_state extends boolean = boolean>(
    token: string,
    parameters: {
        /** Describes which categories to search in and their criteria. */
        search_categories: {
            /** Mapping of category name to search criteria. */
            room_events?: {
                /** Configures whether any context for the events returned are included in the response. */
                event_context?: {
                    /**
                     * How many events after the result are returned.
                     * By default, this is `5`.
                     */
                    after_limit?: number;
                    /**
                     * How many events before the result are returned.
                     * By default, this is `5`.
                     */
                    before_limit?: number;
                    /**
                     * Requests that the server returns the historic profile information for the users that sent the events that were returned.
                     * By default, this is `false`.
                     */
                    include_profile?: boolean;
                };
                /** This takes a `filter`_. */
                filter?: {
                    /** The maximum number of events to return. */
                    limit?: number;
                    /**
                     * A list of sender IDs to exclude. If this list is absent then no senders are excluded.
                     * A matching sender will be excluded even if it is listed in the `'senders'` filter.
                     */
                    not_senders?: string[];
                    /**
                     * A list of event types to exclude. If this list is absent then no event types are excluded.
                     * A matching type will be excluded even if it is listed in the `'types'` filter.
                     * A '*' can be used as a wildcard to match any sequence of characters.
                     */
                    not_types?: string[];
                    /**
                     * A list of senders IDs to include.
                     * If this list is absent then all senders are included.
                     */
                    senders?: string[];
                    /**
                     * A list of event types to include.
                     * If this list is absent then all event types are included.
                     * A `'*'` can be used as a wildcard to match any sequence of characters.
                     */
                    types?: string[];
                    /**
                     * If `true`, includes only events with a `url` key in their content.
                     * If `false`, excludes those events.
                     * If omitted, `url` key is not considered for filtering.
                     */
                    contains_url?: boolean;
                    /**
                     * If `true`, sends all membership events for all events, even if they have already been sent to the client.
                     * Does not apply unless `lazy_load_members` is `true`.
                     * See `Lazy-loading room members`_ for more information.
                     * Defaults to `false`.
                     */
                    include_redundant_members?: boolean;
                    /**
                     * If `true`, enables lazy-loading of membership events.
                     * See `Lazy-loading room members`_ for more information.
                     * Defaults to `false`.
                     */
                    lazy_load_members?: boolean;
                    /**
                     * A list of room IDs to exclude.
                     * If this list is absent then no rooms are excluded.
                     * A matching room will be excluded even if it is listed in the `'rooms'` filter.
                     */
                    not_rooms?: string[];
                    /**
                     * A list of room IDs to include.
                     * If this list is absent then all rooms are included.
                     */
                    rooms?: string[];
                };
                /** Requests that the server partitions the result set based on the provided list of keys. */
                groupings?: {
                    /** List of groups to request. */
                    group_by?: {
                        /** Key that defines the group. */
                        key?: "room_id" | "sender";
                    }[];
                };
                /** Requests the server return the current state for each room returned. */
                include_state?: include_state;
                /**
                 * The keys to search.
                 * Defaults to all.
                 */
                keys?: ("content.body" | "content.name" | "content.topic")[];
                /**
                 * The order in which to search for results.
                 * By default, this is `"rank"`.
                 */
                order_by?: "recent" | "rank";
                /** The string to search events for */
                search_term: string;
            };
        };
    },
    nextBatch?: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** Describes which categories to search in and their criteria. */
          search_categories: {
              /** Mapping of category name to search criteria. */
              room_events?: {
                  /** An approximate count of the total number of results found. */
                  count?: number;
                  /**
                   * Any groups that were requested.
                   * The outer `string` key is the group key requested (eg: `room_id` or `sender`).
                   * The inner `string` key is the grouped value (eg: a room's ID or a user's ID).
                   */
                  groups?: {
                      [key in "room_id" | "sender"]?: {
                          [id: string]: {
                              /**
                               * Token that can be used to get the next batch of results in the group, by passing as the _next\_batch_ parameter to the next call.
                               * If this field is absent, there are no more results in this group.
                               */
                              next_batch?: string;
                              /** Key that can be used to order different groups. */
                              order?: number;
                              /** Which results are in this group. */
                              results: string[];
                          };
                      };
                  };
                  /** List of words which should be highlighted, useful for stemming which may change the query terms. */
                  highlights?: string[];
                  /**
                   * Token that can be used to get the next batch of results, by passing as the `next_batch` parameter to the next call.
                   * If this field is absent, there are no more results.
                   */
                  next_batch?: string;
                  /** List of results in the requested order. */
                  results?: {
                      /** Context for result, if requested. */
                      context?: {
                          /** Pagination token for the end of the chunk */
                          end?: string;
                          /** Events just after the result. */
                          events_after?: RoomEvents[];
                          /** Events just before the result. */
                          events_before?: RoomEvents[];
                          /**
                           * The historic profile information of the users that sent the events returned.
                           * The string key is the user ID for which the profile belongs to.
                           */
                          profile_info?: {
                              [userID: string]: {
                                  displayname?: string;
                                  avatar_url?: string;
                              };
                          };
                          start?: string;
                      };
                      /** A number that describes how closely this result matches the search. Higher is closer. */
                      rank?: number;
                      /** The event that matched. */
                      result?: RoomEvents;
                  }[];
                  /**
                   * The current state for every room in the results.
                   * This is included if the request had the `include_state` key set with a value of `true`.
                   * The `string` key is the room ID for which the `State Event` array belongs to.
                   */
                  state?: include_state extends true
                      ? {
                            [roomID: string]: RoomStateEvents[];
                        }
                      : undefined;
              };
          };
      }>
    | RequestError<RateLimitError>
> {
    const url = join(baseURL, endpoints.search) + nextBatch ? `?next_batch=${nextBatch}` : "";
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(url, response, requestInit, "Part of the request was invalid.");
        case 429:
            // Conditional types in Typescript do not work properly as a union type in cases when they are basically a union type
            // This means that the result's search_categories.room_events.state will cause an error
            // Casting the result to any is the only "clean" solution at the moment
            // Since this any type only affects this one line, and this one line only it shouldn't matter too much
            return <any>await handleRateLimitRetry(response, search, [token, parameters, nextBatch, retryDelay, maxRetries, baseURL], retryDelay, maxRetries);
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

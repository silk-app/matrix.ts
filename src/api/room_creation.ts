import { defaultBaseURL, endpoints, errorMessages } from "../constants.ts";
import { join } from "../deps.ts";
import { APIError, RequestError } from "../errors.ts";
import { RoomStateEvents } from "../types/mod.ts";
import { APIResponse } from "../types/mod.ts";
import { generateRequestInit, generateAPIResponse } from "../utils.ts";

/** ## Create Room
 * Create a new room
 *
 * ### Implementation Notes
 * Create a new room with various configuration options.
 *
 * The server MUST apply the normal state resolution rules when creating the new room, including checking power levels for each event.
 * It MUST apply the events implied by the request in the following order:
 *
 * - The `m.room.create` event itself. Must be the first event in the room.
 * - An `m.room.member` event for the creator to join the room. This is needed so the remaining events can be sent.
 * - A default `m.room.power_levels` event, giving the room creator (and not other members) permission to send state events. Overridden by the `power_level_content_override` parameter.
 * - Events set by the `preset`. Currently these are the `m.room.join_rules`, `m.room.history_visibility`, and `m.room.guest_access` state events.
 * - Events listed in `initial_state`, in the order that they are listed.
 * - Events implied by `name` and `topic` (`m.room.name` and `m.room.topic` state events).
 * - Invite events implied by `invite` and `invite_3pid` (`m.room.member` with `membership: invite` and `m.room.third_party_invite`).
 *
 * The available presets do the following with respect to room state:
 *
 * | Preset | `join_rules` | `history_visibility` | `guest_access` | Other |
 * |:------ |:------------ |:-------------------- |:-------------- |:----- |
 * | `private_chat` | `invite` | `shared` | `can_join` |   |
 * | `trusted_private_chat` | `invite` | `shared` | `can_join` | All invites are given the same power level as the room creator. |
 * | `public_chat` | `public` | `shared` | `forbidden` |   |
 *
 * The server will create a `m.room.create` event in the room with the requesting user as the creator, alongside other keys provided in the `creation_content`.
 *
 * @returns Information about the newly created room.
 */
export async function createRoom(
    token: string,
    parameters: {
        /**
         * Extra keys, such as `m.federate`, to be added to the content of the `m.room.create`_ event.
         * The server will clobber the following keys: `creator`, `room_version`.
         * Future versions of the specification may allow the server to clobber other keys.
         */
        creation_content?: Record<string, any>;
        /**
         * A list of state events to set in the new room.
         * This allows the user to override the default state events set in the new room.
         * The expected format of the state events are an object with type, state_key and content keys set.
         *
         * Takes precedence over events set by `preset`, but gets overriden by `name` and `topic` keys.
         */
        initial_state?: Pick<RoomStateEvents, "content" | "state_key" | "type">;
        /**
         * A list of user IDs to invite to the room.
         * This will tell the server to invite everyone in the list to the newly created room.
         */
        invite?: string[];
        /**
         * A list of objects representing third party IDs to invite into the room.
         */
        invite_3pid?: {
            /** The invitee's third party identifier. */
            address: string;
            /**
             * An access token previously registered with the identity server.
             * Servers can treat this as optional to distinguish between r0.5-compatible clients and this specification version.
             */
            id_access_token: string;
            /** The hostname+port of the identity server which should be used for third party identifier lookups. */
            id_server: string;
            /** The kind of address being passed in the address field, for example `email`. */
            medium: string;
        }[];
        /**
         * This flag makes the server set the `is_direct` flag on the `m.room.member` events sent to the users in `invite` and `invite_3pid`.
         * See `Direct Messaging`_ for more information.
         */
        is_direct?: boolean;
        /**
         * If this is included, an `m.room.name` event will be sent into the room to indicate the name of the room.
         * See Room Events for more information on `m.room.name`.
         */
        name?: string;
        /**
         * The power level content to override in the default power level event.
         * This object is applied on top of the generated `m.room.power_levels`_ event content prior to it being sent to the room.
         * Defaults to overriding nothing.
         */
        power_level_content_override?: any;
        /**
         * Convenience parameter for setting various default state events based on a preset.
         *
         * If unspecified, the server should use the visibility to determine which preset to use.
         * A `visbility` of `public` equates to a preset of `public_chat` and `private` visibility equates to a preset of `private_chat`.
         */
        preset?: "private_chat" | "trusted_private_chat" | "public_chat";
        /**
         * The desired room alias local part.
         * If this is included, a room alias will be created and mapped to the newly created room.
         * The alias will belong on the same homeserver which created the room.
         * For example, if this was set to "foo" and sent to the homeserver "example.com" the complete room alias would be `#foo:example.com`.
         *
         * The complete room alias will become the canonical alias for the room.
         */
        room_alias_name?: string;
        /**
         * The room version to set for the room.
         * If not provided, the homeserver is to use its configured default.
         * If provided, the homeserver will return a 400 error with the errcode `M_UNSUPPORTED_ROOM_VERSION` if it does not support the room version.
         */
        room_version?: string;
        /**
         * If this is included, an `m.room.topic` event will be sent into the room to indicate the topic for the room.
         * See Room Events for more information on `m.room.topic`.
         */
        topic?: string;
        /**
         * A `public` visibility indicates that the room will be shown in the published room list.
         * A `private` visibility will hide the room from the published room list.
         * Rooms default to `private` visibility if this key is not included.
         * NB: This should not be confused with `join_rules` which also uses the word `public`.
         */
        visibility?: "public" | "private";
    },
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The created room's ID. */
          room_id: string;
      }>
    | RequestError<APIError>
> {
    const url = join(baseURL, endpoints.createRoom);
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(
                url,
                response,
                requestInit,
                `The request is invalid.
A meaningful \`errcode\` and description error text will be returned.
Example reasons for rejection include:

- The request body is malformed (\`errcode\` set to \`M_BAD_JSON\` or \`M_NOT_JSON\`).
- The room alias specified is already taken (\`errcode\` set to \`M_ROOM_IN_USE\`).
- The initial state implied by the parameters to the request is invalid: for example, the user's \`power_level\` is set below that necessary to set the room name (\`errcode\` set to \`M_INVALID_ROOM_STATE\`).
- The homeserver doesn't support the requested room version, or one or more users being invited to the new room are residents of a homeserver which does not support the requested room version. The \`errcode\` will be \`M_UNSUPPORTED_ROOM_VERSION\` in these cases.`,
                await response.json()
            );
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

import { RateLimitError, RequestError } from "../errors.ts";
import { APIResponse, m } from "../types/mod.ts";
import { join } from "../deps.ts";
import { generateAPIResponse, generateRequestInit, handleRateLimitRetry } from "../utils.ts";
import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";

/** ## Set Read Marker
 * Set the position of the read marker for a room.
 *
 * ### Implementation Notes
 * Sets the position of the read marker for a given room, and optionally the read receipt's location.
 *
 * @param roomID The room ID to set the read marker in for the user.
 * @returns The read marker, and read receipt if provided, have been updated.
 */
export async function setReadMarker(
    token: string,
    roomID: string,
    parameters: {
        /**
         * The event ID the read marker should be located at.
         * The event MUST belong to the room
         */
        "m.fully_read": string;
        /**
         * The event ID to set the read receipt location at.
         * This is equivalent to calling `/receipt/m.read/$elsewhere:example.org` and is provided here to save that extra call.
         */
        "m.read"?: string;
    },
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<APIResponse<void> | RequestError<RateLimitError>> {
    const url = join(baseURL, endpoints.readMarker(roomID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, setReadMarker, [token, roomID, parameters, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

import { endpoints, defaultBaseURL, errorMessages } from "../constants.ts";
import { join } from "../deps.ts";
import { APIError, RequestError } from "../errors.ts";
import { APIResponse } from "../types/mod.ts";
import { generateAPIResponse, generateRequestInit } from "../utils.ts";

/** ## Upgrade Room
 * Upgrades a room to a new room version.
 *
 * ### Implementation Notes
 * Upgrades the given room to a particular room version.
 *
 * @param roomID The ID of the room to upgrade.
 * @returns The room was successfully upgraded.
 */
export async function upgradeRoom(
    token: string,
    roomID: string,
    parameters: {
        /** The new version for the room. */
        new_version: string;
    },
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The ID of the new room. */
          replacement_room: string;
      }>
    | RequestError<APIError>
> {
    const url = join(baseURL, endpoints.upgradeRoom(roomID));
    const requestInit = generateRequestInit("POST", token, parameters);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 400:
            return new RequestError(
                url,
                response,
                requestInit,
                "The request was invalid. One way this can happen is if the room version requested is not supported by the homeserver.",
                await response.json()
            );
        case 403:
            return new RequestError(url, response, requestInit, "The user is not permitted to upgrade the room.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

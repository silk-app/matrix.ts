import { defaultBaseURL, defaultMaxRetries, defaultRetryDelay, endpoints, errorMessages } from "../constants.ts";
import { join } from "../deps.ts";
import { APIError, RateLimitError, RequestError } from "../errors.ts";
import { APIResponse, FileAPIResponse } from "../types/mod.ts";
import { generateAPIResponse, generateFileAPIResponse, generateQueriedURL, generateRequestInit, handleRateLimitRetry } from "../utils.ts";

/** ## Get Config
 * Get the configuration for the content repository.
 *
 * ### Implementation Notes
 * This endpoint allows clients to retrieve the configuration of the content repository, such as upload limitations.
 * Clients SHOULD use this as a guide when using content repository endpoints.
 * All values are intentionally left optional.
 * Clients SHOULD follow the advice given in the field description when the field is not available.
 *
 * **NOTE:** Both clients and server administrators should be aware that proxies between the client and the server may affect the apparent behaviour of content repository APIs,
 * for example, proxies may enforce a lower upload size limit than is advertised by the server on this endpoint.
 *
 * @returns The public content repository configuration for the matrix server.
 */
export async function getConfig(
    token: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /**
           * The maximum size an upload can be in bytes.
           * Clients SHOULD use this as a guide when uploading content.
           * If not listed or null, the size limit should be treated as unknown.
           */
          "m.upload.size"?: number;
      }>
    | RequestError<RateLimitError>
> {
    const url = join(baseURL, endpoints.mediaConfig);
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, getConfig, [token, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Content
 * Download content from the content repository.
 *
 * @param serverName The server name from the `mxc://` URI (the authoritory component)
 * @param mediaID The media ID from the `mxc://` URI (the path component)
 * @param allowRemote Indicates to the server that it should not attempt to fetch the media if it is deemed remote. This is to prevent routing loops where the server contacts itself. Defaults to true if not provided.
 * @returns file
 */
export async function getContent(
    serverName: string,
    mediaID: string,
    allowRemote?: boolean,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<FileAPIResponse | RequestError<APIError>> {
    const url = generateQueriedURL(join(baseURL, endpoints.mediaContent(serverName, mediaID)), { allow_remote: allowRemote });
    const requestInit = generateRequestInit("GET");
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateFileAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, getContent, [serverName, mediaID, allowRemote, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        case 502:
            return new RequestError(url, response, requestInit, "The content is too large for the server to serve.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Content Override Name
 * Download content from the content repository.
 * This is the same as the download endpoint above, except permitting a desired file name.
 *
 * @param serverName The server name from the `mxc://` URI (the authoritory component)
 * @param mediaID The media ID from the `mxc://` URI (the path component)
 * @param fileName A filename to give in the `Content-Disposition` header.
 * @param allowRemote Indicates to the server that it should not attempt to fetch the media if it is deemed remote. This is to prevent routing loops where the server contacts itself. Defaults to true if not provided.
 * @returns file
 */
export async function getContentOverrideName(
    serverName: string,
    mediaID: string,
    fileName: string,
    allowRemote?: boolean,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<FileAPIResponse | RequestError<APIError>> {
    const url = generateQueriedURL(join(baseURL, endpoints.mediaContentOverrideName(serverName, mediaID, fileName)), { allow_remote: allowRemote });
    const requestInit = generateRequestInit("GET");
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateFileAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(
                    response,
                    getContentOverrideName,
                    [serverName, mediaID, fileName, allowRemote, 0, 0, baseURL],
                    retryDelay,
                    maxRetries
                );
            } catch (error) {
                return error;
            }
        case 502:
            return new RequestError(url, response, requestInit, "The content is too large for the server to serve.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get URL Preview
 * Get information about a URL for a client
 *
 * ### Implementation Notes
 * Get information about a URL for the client.
 * Typically this is called when a client sees a URL in a message and wants to render a preview for the user.
 *
 * .. Note:: Clients should consider avoiding this endpoint for URLs posted in encrypted rooms.
 * Encrypted rooms often contain more sensitive information the users do not want to share with the homeserver, and this can mean that the URLs being shared should also not be shared with the homeserver.
 *
 * @param url The URL to get a preview of.
 * @param ts The preferred point in time to return a preview for. The server may return a newer version if it does not have the requested version available.
 * @returns The OpenGraph data for the URL, which may be empty. Some values are replaced with matrix equivalents if they are provided in the response. The differences from the OpenGraph protocol are described here.
 */
export async function getURLPreview(
    token: string,
    url: string,
    ts?: number | BigInt,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The byte-size of the image. Omitted if there is no image attached. */
          "matrix:image:size"?: number;
          /** An `MXC URI`_ to the image. Omitted if there is no image. */
          "og:image"?: string;
      }>
    | RequestError<RateLimitError>
> {
    const _url = generateQueriedURL(join(baseURL, endpoints.previewURL), { url, ts: ts?.toString() });
    const requestInit = generateRequestInit("GET", token);
    const response = await fetch(_url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 429:
            try {
                return await handleRateLimitRetry(response, getURLPreview, [token, url, ts, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Get Content Thumbnail
 * Download a thumbnail of content from the content repository. See the thumbnailing section for more information.
 *
 * @param serverName The server name from the `mxc://` URI (the authoritory component)
 * @param mediaID The media ID from the `mxc://` URI (the path component)
 * @param width The desired width of the thumbnail. The actual thumbnail may be larger than the size specified.
 * @param height The desired height of the thumbnail. The actual thumbnail may be larger than the size specified.
 * @param method The desired resizing method. See the thumbnailing section for more information.
 * @param allowRemote Indicates to the server that it should not attempt to fetch the media if it is deemed remote. This is to prevent routing loops where the server contacts itself. Defaults to true if not provided.
 * @returns file
 */
export async function getContentThumbnail(
    serverName: string,
    mediaID: string,
    width: number,
    height: number,
    method?: "scale" | "crop",
    allowRemote?: boolean,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<FileAPIResponse | RequestError<APIError | RateLimitError>> {
    const url = generateQueriedURL(join(baseURL, endpoints.contentThumbnail(serverName, mediaID)), {
        width,
        height,
        method,
        allow_remote: allowRemote
    });
    const requestInit = generateRequestInit("GET");
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateFileAPIResponse(response);
        case 400:
            return new RequestError(
                url,
                response,
                requestInit,
                "The request does not make sense to the server, or the server cannot thumbnail the content." +
                    "For example, the client requested non-integer dimensions or asked for negatively-sized images.",
                await response.json()
            );
        case 413:
            return new RequestError(url, response, requestInit, "The local content is too large for the server to thumbnail.", await response.json());
        case 429:
            try {
                return await handleRateLimitRetry(
                    response,
                    getContentThumbnail,
                    [serverName, mediaID, width, height, method, allowRemote, 0, 0, baseURL],
                    retryDelay,
                    maxRetries
                );
            } catch (error) {
                return error;
            }
        case 502:
            return new RequestError(url, response, requestInit, "The remote content is too large for the server to thumbnail.", await response.json());
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

/** ## Upload Content
 * Upload some content to the content repository
 *
 * @param blob The blob of the file
 * @param contentType The content type of the file (mimetype)
 * @param filename The filename of the file
 * @returns The `MXC URI`_ for the uploaded content.
 */
export async function uploadContent(
    token: string,
    blob: Blob,
    contentType?: string,
    filename?: string,
    retryDelay: number = defaultRetryDelay,
    maxRetries: number = defaultMaxRetries,
    baseURL: string = defaultBaseURL
): Promise<
    | APIResponse<{
          /** The `MXC URI`_ to the uploaded content. */
          content_uri: string;
      }>
    | RequestError<APIError | RateLimitError>
> {
    const url = generateQueriedURL(join(baseURL, endpoints.uploadContent), { filename });
    const requestInit = generateRequestInit("POST", token, blob, contentType);
    const response = await fetch(url, requestInit);
    switch (response.status) {
        case 200:
            return generateAPIResponse(response);
        case 403:
            return new RequestError(
                url,
                response,
                requestInit,
                `The user does not have permission to upload the content. Some reasons for this error include:

- The server does not permit the file type.
- The user has reached a quota for uploaded content.`,
                await response.json()
            );
        case 413:
            return new RequestError(url, response, requestInit, "The uploaded content is too large for the server.", await response.json());
        case 429:
            try {
                return await handleRateLimitRetry(response, uploadContent, [token, blob, contentType, filename, 0, 0, baseURL], retryDelay, maxRetries);
            } catch (error) {
                return error;
            }
        default:
            return new RequestError(url, response, requestInit, errorMessages.statusCodeNotImplemented);
    }
}

// deno-lint-ignore-file camelcase

export type APIErrorCode =
    | "M_FORBIDDEN"
    | "M_UNKNOWN_TOKEN"
    | "M_MISSING_TOKEN"
    | "M_BAD_JSON"
    | "M_NOT_JSON"
    | "M_NOT_FOUND"
    | "M_LIMIT_EXCEEDED"
    | "M_UNKNOWN"
    | "M_UNRECOGNIZED"
    | "M_UNAUTHORIZED"
    | "M_USER_DEACTIVATED"
    | "M_USER_IN_USE"
    | "M_INVALID_USERNAME"
    | "M_ROOM_IN_USE"
    | "M_INVALID_ROOM_STATE"
    | "M_THREEPID_IN_USE"
    | "M_THREEPID_NOT_FOUND"
    | "M_THREEPID_AUTH_FAILED"
    | "M_THREEPID_DENIED"
    | "M_SERVER_NOT_TRUSTED"
    | "M_UNSUPPORTED_ROOM_VERSION"
    | "M_INCOMPATIBLE_ROOM_VERSION"
    | "M_BAD_STATE"
    | "M_GUEST_ACCESS_FORBIDDEN"
    | "M_CAPTCHA_NEEDED"
    | "M_CAPTCHA_INVALID"
    | "M_MISSING_PARAM"
    | "M_INVALID_PARAM"
    | "M_TOO_LARGE"
    | "M_EXCLUSIVE"
    | "M_RESOURCE_LIMIT_EXCEEDED"
    | "M_CANNOT_LEAVE_SERVER_NOTICE_ROOM";

/** An interface to represent API Errors */
export interface APIError<ErrorCode extends APIErrorCode = APIErrorCode> {
    errcode: ErrorCode;
    error?: string;
}

/** A type to shorten down some code */
export type APIErrorExcept<ErrorCode extends APIErrorCode> = APIError<Exclude<APIErrorCode, ErrorCode>>;

export interface RateLimitError extends APIError<"M_LIMIT_EXCEEDED"> {
    /** The amount of time in milliseconds the client should wait before trying the request again */
    retry_after_ms?: number;
}

export interface UnauthorizedError {
    /** A list of the stages the client has completed successfully. */
    completed?: string[];
    /** A list of the login flows supported by the server for this API. */
    flows: {
        /** The login type of each of the stages required to complete this authentication flow. */
        stages: string[];
    }[];
    /**
     * Contains any information that the client will need to know in order to use a given type of authentication.
     * For each login type presented, that type may be present as a key in this dictionary.
     * For example, the public part of an OAuth client ID could be given here
     */
    params?: any;
    /** This is a session identifier that the client must pass back to the home server, if one is provided, in subsequent attempts to authenticate in the same API call. */
    session?: string;
}

/** A request error class (so it can be used with `instanceof`) */
export class RequestError<T = undefined> extends Error {
    url: string;
    response: Response;
    requestInit?: RequestInit;
    error?: T;
    constructor(url: string, response: Response, requestInit?: RequestInit, message?: string, error?: T) {
        super(message);
        this.url = url;
        this.requestInit = requestInit;
        this.response = response;
        this.error = error;
    }
    get status() {
        return this.response.status;
    }
}

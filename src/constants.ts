export const defaultBaseURL = "https://matrix.org/";
export const defaultRetryDelay = 2000;
export const defaultMaxRetries = 5;

export const endpoints = {
    // Server Administration
    wellKnown: "/.well-known/matrix/client",
    whoIs: (userID: string) => `/_matrix/client/r0/admin/whois/${userID}`,
    versions: "/_matrix/client/versions",

    // User Data
    account3PIDs: "/_matrix/client/r0/account/3pid",
    bind3PID: "/_matrix/client/r0/account/3pid/bind",
    delete3PIDFromAccount: "/_matrix/client/r0/account/3pid/delete",
    unbind3PIDFromAccount: "/_matrix/client/r0/account/3pid/unbind",
    deactivateAccount: "/_matrix/client/r0/account/deactivate",
    changePassword: "/_matrix/client/r0/account/password",
    getTokenOwner: "/_matrix/client/r0/account/whoami",
    userProfile: (userID: string) => `/_matrix/client/r0/profile/${userID}`,
    avatarURL: (userID: string) => `/_matrix/client/r0/profile/${userID}/avatar_url`,
    displayName: (userID: string) => `/_matrix/client/r0/profile/${userID}/displayname`,
    register: "/_matrix/client/r0/register",
    checkUsernameAvailability: "/_matrix/client/r0/register/available",
    accountData: (userID: string, type: string) => `/_matrix/client/r0/user/${userID}/account_data/${type}`,
    accountDataPerRoom: (userID: string, roomID: string, type: string) => `/_matrix/client/r0/user/${userID}/rooms/${roomID}/account_data/${type}`,
    roomTags: (userID: string, roomID: string) => `/_matrix/client/r0/user/${userID}/rooms/${roomID}/tags`,
    roomTag: (userID: string, roomID: string, tag: string) => `/_matrix/client/r0/user/${userID}/rooms/${roomID}/tags/${tag}`,
    searchUserDirectory: "/_matrix/client/r0/user_directory/search",

    // Capabilities
    capabilities: "/_matrix/client/r0/capabilities",

    // Room Creation
    createRoom: "/_matrix/client/r0/createRoom",

    // Device Management
    deleteDevices: "/_matrix/client/r0/delete_devices",
    devices: "/_matrix/client/r0/devices",
    device: (deviceID: string) => `/_matrix/client/r0/devices/${deviceID}`,

    // Application Service Room Directory Management
    appServiceRoomDirectoryVisibility: (networkID: string, roomID: string) => `/_matrix/client/r0/directory/list/appservice/${networkID}/${roomID}`,

    // Room Directory
    roomAlias: (roomAlias: string) => `/_matrix/client/r0/directory/room/${roomAlias}`,
    localAliases: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/aliases`,

    // End-to-end Encryption
    keysChanges: "/_matrix/client/r0/keys/changes",
    claimKeys: "/_matrix/client/r0/keys/claim",
    queryKeys: "/_matrix/client/r0/keys/query",
    uploadKeys: "/_matrix/client/r0/keys/upload",

    // Session Management
    login: "/_matrix/client/r0/login",
    logout: "/_matrix/client/r0/logout",
    logoutAll: "/_matrix/client/r0/logout/all",

    // Push Notifications
    notifications: "/_matrix/client/r0/notifications",
    pushers: "/_matrix/client/r0/pushers",
    setPusher: "/_matrix/client/r0/pushers/set",
    pushRules: "/_matrix/client/r0/pushrules",
    pushRule: (scope: string, kind: string, ruleID: string) => `/_matrix/client/r0/pushrules/${scope}/${kind}/${ruleID}`,
    pushRuleActions: (scope: string, kind: string, ruleID: string) => `/_matrix/client/r0/pushrules/${scope}/${kind}/${ruleID}/actions`,
    pushRuleEnabled: (scope: string, kind: string, ruleID: string) => `/_matrix/client/r0/pushrules/${scope}/${kind}/${ruleID}/enabled`,

    // Presence
    presence: (userID: string) => `/_matrix/client/r0/presence/${userID}/status`,

    // Room Discovery
    publicRooms: "/_matrix/client/r0/publicRooms",

    // Room Participation
    events: "/_matrix/client/r0/events",
    event: (eventID: string) => `/_matrix/client/r0/events/${eventID}`,
    initialSync: "/_matrix/client/r0/initialSync",
    eventContext: (roomID: string, eventID: string) => `/_matrix/client/r0/rooms/${roomID}/context/${eventID}`,
    roomEvent: (roomID: string, eventID: string) => `/_matrix/client/r0/rooms/${roomID}/event/${eventID}`,
    roomInitialSync: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/initialSync`,
    roomJoinedMembers: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/joined_members`,
    roomMembers: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/members`,
    roomEvents: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/messages`,
    receipt: (roomID: string, receiptType: string, eventID: string) => `/_matrix/client/r0/rooms/${roomID}/receipt/${receiptType}/${eventID}`,
    redactEvent: (roomID: string, eventID: string, txnID: string) => `/_matrix/client/r0/rooms/${roomID}/redact/${eventID}/${txnID}`,
    sendMessage: (roomID: string, eventType: string, txnID: string) => `/_matrix/client/r0/rooms/${roomID}/send/${eventType}/${txnID}`,
    roomState: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/state`,
    roomStateWithKey: (roomID: string, eventType: string, stateKey: string) => `/_matrix/client/r0/rooms/${roomID}/state/${eventType}/${stateKey}`,
    typing: (roomID: string, userID: string) => `/_matrix/client/r0/rooms/${roomID}/typing/${userID}`,
    sync: "/_matrix/client/r0/sync",
    defineFilter: (userID: string) => `/_matrix/client/r0/user/${userID}/filter`,
    filter: (userID: string, filterID: string) => `/_matrix/client/r0/user/${userID}/filter/${filterID}`,

    // Room Membership
    joinRoomByIDOrAlias: (roomIDOrAlias: string) => `/_matrix/client/r0/join/${roomIDOrAlias}`,
    joinedRooms: "/_matrix/client/r0/joined_rooms",
    ban: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/ban`,
    forgetRoom: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/forget`,
    invite: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/invite`,
    joinRoomByID: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/join`,
    kick: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/kick`,
    leave: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/leave`,
    unban: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/unban`,

    // Read Markers
    readMarker: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/read_markers`,

    // Reporting Content
    reportContent: (roomID: string, eventID: string) => `/_matrix/client/r0/rooms/${roomID}/report/${eventID}`,

    // Room Upgrades
    upgradeRoom: (roomID: string) => `/_matrix/client/r0/rooms/${roomID}/upgrade`,

    // Search
    search: "/_matrix/client/r0/search",

    // Send-to-Device Messaging
    sendToDevice: (eventType: string, txnId: string) => `/_matrix/client/r0/sendToDevice/${eventType}/${txnId}`,

    // OpenID
    requestOpenIdToken: (userID: string) => `/_matrix/client/r0/user/${userID}/openid/request_token`,

    // VOIP
    turnServer: "/_matrix/client/r0/voip/turnServer",

    // Media
    mediaConfig: "/_matrix/media/r0/config",
    mediaContent: (serverName: string, mediaID: string) => `/_matrix/media/r0/download/${serverName}/${mediaID}`,
    mediaContentOverrideName: (serverName: string, mediaID: string, fileName: string) => `/_matrix/media/r0/download/${serverName}/${mediaID}/${fileName}`,
    previewURL: "/_matrix/media/r0/preview_url",
    contentThumbnail: (serverName: string, mediaID: string) => `/_matrix/media/r0/thumbnail/${serverName}/${mediaID}`,
    uploadContent: "/_matrix/media/r0/upload"
};

export const errorMessages = {
    statusCodeNotImplemented: "Status code not implemented"
};

export enum VerificationErrorCodes {
    /** The user cancelled the verification. */
    USER = "m.user",
    /**
     * The verification process timed out.
     * Verification processes can define their own timeout parameters.
     */
    TIMEOUT = "m.timeout",
    /** The device does not know about the given transaction ID. */
    UNKNOWN_TRANSACTION = "m.unknown_transaction",
    /**
     * The device does not know how to handle the requested method.
     * This should be sent for `m.key.verification.start` messages and messages defined by individual verification processes.
     */
    UNKNOWN_METHOD = "m.unknown_method",
    /**
     * The device received an unexpected message.
     * Typically raised when one of the parties is handling the verification out of order.
     */
    UNEXPECTED_MESSAGE = "m.unexpected_message",
    /** The key was not verified. */
    KEY_MISMATCH = "m.key_mismatch",
    /** The expected user did not match the user verified. */
    USER_MISMATCH = "m.user_mismatch",
    /** The message received was invalid. */
    INVALID_MESSAGE = "m.invalid_message",
    /**
     * A `m.key.verification.request` was accepted by a different device.
     * The device receiving this error can ignore the verification request.
     */
    ACCEPTED = "m.accepted"
}

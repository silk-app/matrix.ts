# Matrix API

[![nest.land](https://nest.land/badge-block.svg)](https://nest.land/package/matrix.ts) [![pipeline](https://gitlab.com/silk-matrix/matrix.ts/badges/master/pipeline.svg)](https://gitlab.com/silk-matrix/matrix.ts/pipelines)

A typescript module for the matrix.org API, designed solely around the matrix.org spec.

Currently only supports client-server API, documentation is not available; however, all useful types and functions have JSDocs that were taken from the matrix.org client-server spec documentation.

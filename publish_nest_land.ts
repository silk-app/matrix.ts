import { publish } from "https://x.nest.land/eggs@0.3.5/src/commands/publish.ts";
import { writeAPIKey } from "https://x.nest.land/eggs@0.3.5/src/keyfile.ts";

const apiKey = Deno.env.get("NEST_LAND_API_KEY");
const isUnstable = Boolean(Deno.env.get("MODULE_UNSTABLE"));
const version = Deno.env.get("CI_COMMIT_TAG");

if (!apiKey) {
    throw new Error("No API Key found in NEST_LAND_API_KEY variable");
} else if (!version) {
    throw new Error("No version found in CI_COMMIT_TAG variable");
} else {
    await writeAPIKey(apiKey);
}

await publish(
    {
        entry: "./mod.ts",
        description: "A typescript module for the matrix.org API, designed solely around the matrix.org spec.",
        homepage: "https://gitlab.com/silk-matrix/matrix.ts",
        unstable: isUnstable,
        unlisted: true,
        version: version,
        files: ["README.md", "LICENSE", "./mod.ts", "./src/**/*"],
        checkFormat: false,
        checkTests: false,
        checkInstallation: false,
        check: false,
        yes: true
    },
    "matrix.ts"
);
